Install
=============
To install the project, simply run in your terminal:

	git clone git@gitlab.com:idiosuite/site-admin.git
	npm install  
	composer install
	edit phinx.yml
	vendor/bin/phinx migrate  
	mkdir public/uploads
	chmod 777 public/uploads

Then:

*	Duplicate config.example.php to config.php 