<?php

namespace App\FrontEnd;

use \PHPUnit\Framework\TestCase;

use \org\bovigo\vfs\vfsStream;
use \Symfony\Component\Yaml\Yaml;

class ContentProviderTest extends TestCase
{
	private $preferences;
	private $contacts;
	private $root;

	/** @before */
	protected function setUpVfsStream()
	{
		$this->preferences = ['menu' => ['info' => 'Info'], 'title' => 'Foobar', 'staffOrder' => ['john-doe', 'jane-doe']];
		$this->contacts = ['foo' => 'bar'];

		$this->root = vfsStream::setup('root', null, [
			'content' => [
				'preferences.txt' => Yaml::dump($this->preferences),
				'contacts.txt' => Yaml::dump($this->contacts),
				'info.txt' => 'Info',
				'jane-doe.txt' => 'Jane Doe',
				'john-doe.txt' => 'John Doe',
				'services.txt' => 'Services',
				'mission.txt' => 'Mission',
			]
		]);
	}
	
	/** @before */
	protected function setUpContentProvider()
	{
		ContentProvider::setBasePath('vfs://root/content');
		ContentProvider::setDriver('static');
	}

	/** @after */
	protected function tearDownContentProvider()
	{
		ContentProvider::reset();
	}

	/** @test */
	public function it_can_be_intantiated()
	{
		$this->assertNotNull(new ContentProvider);
	}

	/** @test */
	public function it_returns_preferences()
	{
		$this->assertEquals(ContentProvider::preferences(), $this->preferences);
	}

	/** @test */
	public function it_returns_preference_key()
	{
		$this->assertEquals(ContentProvider::preferences('title'), 'Foobar');
	}

	/** @test */
	public function it_returns_info_page_by_slug()
	{
		$info = ContentProvider::info();
		$this->assertInstanceOf(\App\Domain\Page::class, $info);
		$this->assertEquals('Info', $info->body);
	}

	/** @test */
	public function it_returns_services_page_by_slug()
	{
		$services = ContentProvider::services();
		$this->assertInstanceOf(\App\Domain\Page::class, $services);
		$this->assertEquals('Services', $services->body);
	}

	/** @test */
	public function it_returns_staff_members_according_to_order_specified_via_preferences()
	{
		$profilesArray = ContentProvider::staffList();
		$this->assertCount(2, $profilesArray);
		$this->assertEquals('John Doe', $profilesArray[0]->body);
		$this->assertEquals('Jane Doe', $profilesArray[1]->body);
	}

	/** @test */
	public function it_returns_staff_members_according_to_order_specified_via_preferences_with_array()
	{
		$this->preferences['staffOrder'] = [
			'studio-ceruti' => ['jane-doe'],
			'studio-piona' => ['john-doe'],
		];
		file_put_contents('vfs://root/content/preferences.txt', Yaml::dump($this->preferences));
		
		$profilesArray = ContentProvider::staffList();
		
		$this->assertCount(2, $profilesArray);
		$this->assertEquals(['studio-ceruti', 'studio-piona'], array_keys($profilesArray));
		$this->assertCount(1, $profilesArray['studio-ceruti']);
		$this->assertEquals('jane-doe', $profilesArray['studio-ceruti'][0]->slug);
		$this->assertCount(1, $profilesArray['studio-piona']);
		$this->assertEquals('john-doe', $profilesArray['studio-piona'][0]->slug);
	}

	/** @test */
	public function it_returns_staff_profile_by_slug()
	{
		$_GET['slug'] = 'john-doe';
		$profile = ContentProvider::staffProfile();
		$this->assertInstanceOf(\App\Domain\Page::class, $profile);
		$this->assertEquals('John Doe', $profile->body);
	}

	/** @test */
	public function it_returns_contacts()
	{
		$this->assertEquals($this->contacts, ContentProvider::contacts());
	}

	/** @test */
	public function it_returns_info_via_get_method()
	{
		$info = ContentProvider::get('info');
		$this->assertInstanceOf(\App\Domain\Page::class, $info);
		$this->assertEquals('Info', $info->body);
	}

	/** @test */
	public function it_returns_unregistered_page()
	{
		$mission = ContentProvider::get('mission');
		$this->assertInstanceOf(\App\Domain\Page::class, $mission);
		$this->assertEquals('Mission', $mission->body);
	}
}