<?php

namespace Tests\Unit\App\Api;

use \App\Testing\AdminTestCase;
use \App\Api\ApiRouter;

class ApiRouterTest extends AdminTestCase
{
    /** @test */
	public function it_can_be_instantiated()
	{
        $instance = new ApiRouter();

        $this->assertNotNull($instance);
	}

    /** @test */
	public function it_can_register_a_route()
	{
        $instance = new ApiRouter();

        $this->assertFalse($instance->has('/api/news'));

        $instance->register('/api/news', 'hello world');

        $this->assertTrue($instance->has('/api/news'));
	}

    /** @test */
	public function it_returns_invalid_response_if_route_is_not_registered()
	{
        $instance = new ApiRouter();

        $this->assertEquals(
            json_encode([
                'ok' => false,
                'message' => 'Unknown api endpoint',
                'data' => [],
            ]), 
            $instance->getResponse('/api/foobar')
        );
	}

    /** @test */
	public function it_returns_string_response()
	{
        $instance = new ApiRouter();

        $instance->register('/api/news', 'hello world');

        $this->assertEquals('hello world', $instance->getResponse('/api/news'));
	}

    /** @test */
	public function it_returns_response_for_callable_array_with_domain_object()
	{
        $instance = new ApiRouter();

        $instance->register('/api/news', [\App\Domain\News::class, 'getAllPublished']);

        $this->assertEquals(
            json_encode([
                'ok' => true,
                'message' => '',
                'data' => [],
            ]), 
            $instance->getResponse('/api/news')
        );
	}

    /** @test */
	public function it_returns_callable_array_with_instance_response()
	{
        $instance = new ApiRouter();

        $instance->register('/api/news', [new \App\Api\NewsResponseFactory, 'getAllPublished']);

        $this->assertEquals(
            json_encode([
                'ok' => true,
                'message' => '',
                'data' => [],
            ]), 
            $instance->getResponse('/api/news')
        );
	}

    /** @test */
	public function it_returns_callable_function_response()
	{
        $instance = new ApiRouter();

        $instance->register('/api/news', function () {
            return \App\Api\ResponseFactory::make(\App\Domain\News::class)->getAllPublished();
        });

        $this->assertEquals(
            json_encode([
                'ok' => true,
                'message' => '',
                'data' => [],
            ]), 
            $instance->getResponse('/api/news')
        );
	}
}