<?php

namespace App\Domain;

use \PHPUnit\Framework\TestCase;

class PageTest extends TestCase
{
	/** @test */
	public function it_can_be_instantiated()
	{
		$page = new Page;
		$this->assertInstanceOf(DomainObject::class, $page);
	}

	/** @test */
	public function it_can_be_instantiated_with_properties()
	{
		$page = new Page([
			'slug' => 'alessandro-piona',
			'title' => 'Dott. Alessandro Piona',
			'heroImage' => 'img/staff/alessandro-piona.jpg',
			'body' => 'Lorem ipsum dolor sit amet',
		]);

		$this->assertEquals('alessandro-piona', $page->slug);
		$this->assertEquals('Dott. Alessandro Piona', $page->title);
		$this->assertEquals('img/staff/alessandro-piona.jpg', $page->heroImage);
		$this->assertEquals('Lorem ipsum dolor sit amet', $page->body);
	}

	/** @test */
	public function it_returns_formatted_body()
	{
		$page = new Page([
			'slug' => 'info',
			'title' => 'Lo studio',
			'heroImage' => null,
			'body' => '-Lorem ipsum dolor sit amet',
		]);

		$this->assertEquals('<p>Lorem ipsum dolor sit amet</p>', $page->getFormattedBody());
	}

	/** @test */
	public function it_returns_formatted_body_for_services()
	{
		$page = new Page([
			'slug' => 'services',
			'title' => 'Lo studio',
			'heroImage' => null,
			'body' => '*Campo 1*' . "\r\n" . 'Foo' . "\r\n" . 'Bar' . "\r\n" . '-Foobar' . "\r\n" . '-Bazbar',
		]);

		$this->assertEquals('<h3>Campo 1</h3><p>Foo</p><p>Bar</p><ul class="Services"><li>Foobar</li><li>Bazbar</li></ul>', $page->getFormattedBody());
	}
}