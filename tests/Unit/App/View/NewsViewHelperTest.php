<?php

namespace App\View;

use \PHPUnit\Framework\TestCase;

use App\Domain\Attachment;
use App\Domain\Collection;
use \App\Domain\News;

class NewsViewHelperTest extends TestCase
{
	/** @test */
	public function it_returns_null_if_the_news_has_no_attachments()
	{
		$news = new News;
		
		$result = NewsViewHelper::attachments($news, 'Scarica l\'allegato');

		$this->assertEquals('', $result);
	}

	/** @test */
	public function it_returns_markup_if_news_has_an_attachment()
	{
		$news = $this->createNewsWithAttachments([
			$this->createRegularAttachment()	
		]);
		
		$result = NewsViewHelper::attachments($news, 'Scarica l\'allegato');

		$this->assertEquals('<a href="http://www.example.com" class="News__attachment">Scarica l\'allegato</a>', $result);
	}

	/** @test */
	public function it_returns_array_if_news_has_an_attachment()
	{
		$news = $this->createNewsWithAttachments([
			$this->createRegularAttachment()	
		]);
		
		$result = NewsViewHelper::attachmentsArray($news->attachments, 'Scarica l\'allegato');

		$this->assertEquals('<a href="http://www.example.com" class="News__attachment">Scarica l\'allegato</a>', $result[0]);
	}

	/** @test */
	public function it_returns_custom_markup_if_it_is_passed()
	{
		$news = $this->createNewsWithAttachments([
			$this->createRegularAttachment()	
		]);
		
		$result = NewsViewHelper::attachments($news, 'Scarica l\'allegato', '<a href="%s">%s</a>');

		$this->assertEquals('<a href="http://www.example.com">Scarica l\'allegato</a>', $result);
	}

	/** @test */
	public function it_will_embed_youtube_video()
	{
		$news = $this->createNewsWithAttachments([
			$this->createYoutubeLinkAttachment()	
		]);
		
		$result = NewsViewHelper::attachments($news, 'Scarica l\'allegato');

		$this->assertEquals('<iframe width="560" height="315" src="https://www.youtube.com/embed/483IAsGUcbg" frameborder="0" allowfullscreen></iframe>', $result);
	}

	/** @test */
	public function it_will_embed_youtube_videos_on_top_of_regular_attachments()
	{
		$news = $this->createNewsWithAttachments([
			$this->createRegularAttachment(),
			$this->createYoutubeLinkAttachment()	
		]);
		
		$result = NewsViewHelper::attachments($news, 'Scarica l\'allegato');

		$this->assertEquals('<iframe width="560" height="315" src="https://www.youtube.com/embed/483IAsGUcbg" frameborder="0" allowfullscreen></iframe> <a href="http://www.example.com" class="News__attachment">Scarica l\'allegato</a>', $result);
	}

	/** @test */
	public function it_will_embed_youtube_videos_on_top_of_regular_attachments_as_array()
	{
		$news = $this->createNewsWithAttachments([
			$this->createRegularAttachment(),
			$this->createYoutubeLinkAttachment()	
		]);
		
		$result = NewsViewHelper::attachmentsArray($news->attachments, 'Scarica l\'allegato');

		$this->assertEquals('<iframe width="560" height="315" src="https://www.youtube.com/embed/483IAsGUcbg" frameborder="0" allowfullscreen></iframe>', $result[0]);
		$this->assertEquals('<a href="http://www.example.com" class="News__attachment">Scarica l\'allegato</a>', $result[1]);
	}

	/** @test */
	public function it_will_wrap_iframe_inside_a_div()
	{
		$news = $this->createNewsWithAttachments([
			$this->createYoutubeLinkAttachment()	
		]);
		
		$attachments = NewsViewHelper::attachments($news, 'Scarica l\'allegato');
		$result = NewsViewHelper::wrapIframe($attachments, 'div', 'News__video');

		$this->assertEquals('<div class="News__video"><iframe width="560" height="315" src="https://www.youtube.com/embed/483IAsGUcbg" frameborder="0" allowfullscreen></iframe></div>', $result);
	}

	/** @test */
	public function it_will_block_cookies_with_iubenda()
	{
		$news = $this->createNewsWithAttachments([
			$this->createYoutubeLinkAttachment()	
		]);
		
		$attachments = NewsViewHelper::attachments($news, 'Scarica l\'allegato');
		$result = NewsViewHelper::blockCookies($attachments);

		$this->assertEquals('<iframe class="_iub_cs_activate" width="560" height="315" suppressedsrc="https://www.youtube.com/embed/483IAsGUcbg" src="//cdn.iubenda.com/cookie_solution/empty.html" frameborder="0" allowfullscreen></iframe>', $result);
	}

	/** @test */
	public function it_will_format_body_with_paragraphs_on_new_lines()
	{
		$news = new News(['body' => 'Lorem ipsum dolor' . "\r\n" . 'sit amet consectetur']);

		$result = NewsViewHelper::formatBody($news, 'News__body');

		$this->assertEquals('<p class="News__body">Lorem ipsum dolor</p><p class="News__body">sit amet consectetur</p>', $result);
	}

	private function createNewsWithAttachments(array $rows)
	{
		$attachmentCollection = $this->createAttachmentCollection($rows);

		$news = new News;
		$news->attachments = $attachmentCollection;

		return $news;
	}

	private function createAttachmentCollection(array $rows)
	{
		return new Collection($rows, new Attachment);
	}

	private function createRegularAttachment()
	{
		return [
			'title' => 'Foobar', 
			'permalink' => 'http://www.example.com', 
			'filename' => 'foobar.jpg', 
			'size' => '1024', 
			'mimetype' => 'image/jpeg'
		];
	}

	private function createYoutubeLinkAttachment()
	{
		return [
			'title' => 'Youtube video', 
			'permalink' => 'https://www.youtube.com/watch?v=483IAsGUcbg', 
			'filename' => '', 
			'size' => '', 
			'mimetype' => ''
		];
	}
}