<?php

namespace App\View;

use \PHPUnit\Framework\TestCase;

use \App\View\Content\Title;
use \App\View\Content\Element;
use \App\View\Content\InlineElement;
use \App\View\Content\Image;
use \App\View\Content\MultilineElement;
use \App\View\Content\Card;
use \App\View\Content\Tag;

class ContentFormatterTest extends TestCase
{
	private $formatter;

	/** @before */
	protected function setUpInstance()
	{
		$this->formatter = new ContentFormatter;
	}

	/** @test */
	public function it_can_be_instantiated()
	{
		$this->assertNotNull($this->formatter);
	}

	/** @test */
	public function it_returns_null_if_no_content_is_set()
	{
		$this->assertNull($this->formatter->get());
	}

	/** @test */
	public function it_returns_formatted_content_if_content_is_set()
	{
		$this->formatter->setContent('Hello world');
		$this->assertEquals('<p>Hello world</p>', $this->formatter->get());
	}

	/** @test */
	public function it_returns_a_single_paragraph_if_two_conesecutive_lines_are_found()
	{
		$this->formatter->setContent('Hello' . "\r\n" . 'world');
		$this->assertEquals('<p>Hello<br/>world</p>', $this->formatter->get());
	}

	/** @test */
	public function it_returns_a_double_paragraph_if_two_consecutive_new_lines_are_found()
	{
		$this->formatter->setContent('Hello' . "\r\n" . "\r\n" . 'World');
		$this->assertEquals('<p>Hello</p><p>World</p>', $this->formatter->get());
	}

	/** @test */
	public function it_returns_h1_when_a_string_between_asterisks_is_found()
	{
		$this->formatter->setContent('*Hello world*');
		$this->assertEquals('<h1>Hello world</h1>', $this->formatter->get());
	}

	/** @test */
	public function it_wont_return_h1_if_no_closing_asterisk_is_found()
	{
		$this->formatter->setContent('*Hello' . "\r\n" . 'world*');
		$this->assertEquals('<p>*Hello<br/>world*</p>', $this->formatter->get());
	}

	/** @test */
	public function it_returns_h2_when_a_string_between_two_asterisks_is_found()
	{
		$this->formatter->setContent('**Hello world**');
		$this->assertEquals('<h2>Hello world</h2>', $this->formatter->get());
	}

	/** @test */
	public function it_provides_a_way_to_customize_filters()
	{
		$this->formatter->setFilters([
			[Title::class, [new Tag('h3', ['class' => 'Staff__jobTitle']), '**']],
		]);
		$this->formatter->setContent('**Dottore commercialista**');
		$this->assertEquals('<h3 class="Staff__jobTitle">Dottore commercialista</h3>', $this->formatter->get());
	}

	/** @test */
	public function it_provides_a_way_to_customize_paragraph_syntax()
	{
		$this->formatter->setFilters([
			[Element::class, [new Tag('p'), '-']],
		]);
		$this->formatter->setContent('-Hello' . "\r\n" . "-Bold" . "\r\n" . '-World');
		$this->assertEquals('<p>Hello</p><p>Bold</p><p>World</p>', $this->formatter->get());
	}

	/** @test */
	public function it_provides_a_way_to_customize_end_token()
	{
		$this->formatter->setFilters([
			[Element::class, [new Tag('p'), '-', '+']],
		]);
		$this->formatter->setContent('-Hello+' . "\r\n" . "-Bold+" . "\r\n" . '-World+');
		$this->assertEquals('<p>Hello</p><p>Bold</p><p>World</p>', $this->formatter->get());
	}

	/** @test */
	public function it_handles_newline_only_content()
	{
		$this->formatter->setContent("\r\n" . "\r\n");
		$this->assertEquals(null, $this->formatter->get());
	}

	/** @test */
	public function it_provides_a_way_to_create_ul()
	{
		$this->formatter->setFilters([
			[MultilineElement::class, [new Tag('ul'), new Tag('li'), '-']],
		]);
		$this->formatter->setContent('-Foo' . "\r\n" . '-Bar' . "\r\n" . '-Baz');
		$this->assertEquals('<ul><li>Foo</li><li>Bar</li><li>Baz</li></ul>', $this->formatter->get());
	}

	/** @test */
	public function it_provides_a_way_to_created_newsted_ul()
	{
		$this->formatter->setFilters([
			[MultilineElement::class, [new Tag('ul'), new Tag('li'), '-', null, $canBeNested = true]],
		]);
		$this->formatter->setContent('-Foo' . "\r\n" . '--Foobar' . "\r\n" . '--Barbaz' . "\r\n" . '---Bazfoo' . "\r\n" . '-Bar' . "\r\n" . '-Baz');
		$this->assertEquals('<ul><li>Foo</li><li><ul><li>Foobar</li><li>Barbaz</li><li><ul><li>Bazfoo</li></ul></li></ul></li><li>Bar</li><li>Baz</li></ul>', $this->formatter->get());
	}

	/** @test */
	public function it_provides_a_way_to_insert_an_image()
	{
	    $this->formatter->setFilters([
			[Image::class, [new Tag('img'), '[img:', ']']],
		]);
		$this->formatter->setContent('[img:_public/_img/image.jpg]');
		$this->assertEquals('<img src="_public/_img/image.jpg">', $this->formatter->get());
	}

	/** @test */
	public function it_provides_a_way_to_insert_an_image_with_custom_class()
	{
	    $this->formatter->setFilters([
			[Image::class, [new Tag('img'), '[img:', ']']],
		]);
		$this->formatter->setContent('[img:_public/_img/image.jpg|class:Foobar|width:500|height:300]');
		$this->assertEquals('<img src="_public/_img/image.jpg" class="Foobar" width="500" height="300">', $this->formatter->get());
	}

	/** @test */
	public function it_provides_a_way_to_group_multiple_items_in_a_card()
	{
	    $this->formatter->setFilters([
			[Element::class, [(new Tag('div', ['class' => 'Card']))->open(), '@start:card']],
			[Element::class, [(new Tag('div'))->close(), '@end:card']],
			[Element::class, [(new Tag('p')), '']],
		]);
		$this->formatter->setContent('
			@start:card
			lorem ipsum dolor sit amet consectetur
			@end:card
		');
		$this->assertEquals(
			'<div class="Card"><p>lorem ipsum dolor sit amet consectetur</p></div>', 
			$this->formatter->get()
		);
	}

	/** @test */
	public function it_provides_a_way_to_group_multiple_items_in_a_card_with_other_filters()
	{
	    $this->formatter->setFilters([
			[Element::class, [(new Tag('div', ['class' => 'Card']))->open(), '@start:card']],
			[Element::class, [(new Tag('div'))->close(), '@end:card']],
			[InlineElement::class, [new Tag('strong'), '*', '*']],
			[Element::class, [(new Tag('p')), '']],
		]);
		$this->formatter->setContent('
			@start:card
			asd *Marco Ceruti:* lorem ipsum dolor sit amet consectetur
			@end:card
		');
		$this->assertEquals(
			'<div class="Card"><p>asd <strong>Marco Ceruti:</strong> lorem ipsum dolor sit amet consectetur</p></div>', 
			$this->formatter->get()
		);
	}

	/** @test */
	public function it_provides_a_way_to_make_text_bold()
	{
	    $this->formatter->setFilters([
			[InlineElement::class, [new Tag('strong'), '**', '**']],
			[Element::class, [(new Tag('p')), '']],
		]);

		$this->formatter->setContent('
			lorem **ipsum dolor** sit amet consectetur
		');
		$this->assertEquals(
			'<p>lorem <strong>ipsum dolor</strong> sit amet consectetur</p>', 
			$this->formatter->get()
		);
	}
}