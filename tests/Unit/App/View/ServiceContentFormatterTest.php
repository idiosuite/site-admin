<?php

namespace App\View;

use \PHPUnit\Framework\TestCase;

use \App\View\Content\Title;
use \App\View\Content\Element;
use \App\View\Content\MultilineElement;
use \App\View\Content\Tag;

class ServiceContentFormatterTest extends TestCase
{
	private $formatter;

	/** @before */
	protected function setUpInstance()
	{
		$this->formatter = ContentFormatterFactory::make()->createServicesFormatter(null);
	}

	/** @test */
	public function it_can_be_instantiated()
	{
		$this->assertNotNull($this->formatter);
	}

	/** @test */
	public function it_parses_services_content()
	{
		$content = "
			*Foobar*
			Foo
			Bar
			Baz
			-Foo
			--Foobar
			-Bar
			-Baz
			*Barbaz*
			**Barfoo**
			@start:cardGroup
			@start:card
			[img:public/_img/image.jpg]
			^Marco Ceruti:^ il pragmatico
			@end:card
			@end:cardGroup
			Baz
			Bar
			Foo
		";
		$markup = $this->formatter->setContent($content)->get();

		$this->assertEquals('<h3>Foobar</h3><p>Foo</p><p>Bar</p><p>Baz</p><ul class="Services"><li>Foo</li><li><ul class="Services"><li>Foobar</li></ul></li><li>Bar</li><li>Baz</li></ul><h3>Barbaz</h3><h4>Barfoo</h4><div class="ServicesCard__container"><div class="ServicesCard"><img src="public/_img/image.jpg"><p><strong>Marco Ceruti:</strong> il pragmatico</p></div></div><p>Baz</p><p>Bar</p><p>Foo</p>', $markup);
	}
}