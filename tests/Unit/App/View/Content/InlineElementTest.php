<?php

namespace App\View;

use \PHPUnit\Framework\TestCase;

use \App\View\Content\InlineElement;
use \App\View\Content\Tag;

class InlineElementTest extends TestCase
{
	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new InlineElement(new Tag('strong'), '*'));
	}

	/** 
	 * @test 
	 * @dataProvider stringMatchProvider
	 */
	public function it_assert_that_a_string_matches($string, $expectMatches)
	{
		$instance = new InlineElement(new Tag('strong'), '*');
	    $this->assertEquals($expectMatches, $instance->matches($string));
	}

	/** 
	 * @test 
	 * @dataProvider stringMarkupProvider
	 */
	public function it_returns_massaged_string($string, $expectString)
	{
		$instance = new InlineElement(new Tag('strong'), '*');
	    $this->assertEquals($expectString, $instance->addLine($string)->get());
	}

	public function stringMatchProvider()
	{
		return [
			['*hello*', $expectMatches = true],
			['*hello', $expectMatches = false],
			['hello', $expectMatches = false],
			['hello*', $expectMatches = false],
			['foo *hello*', $expectMatches = true],
			['foo *hello* bar', $expectMatches = true],
			['*hello* bar', $expectMatches = true],
			['foo *hello* **bar**', $expectMatches = true],
		];
	}

	public function stringMarkupProvider()
	{
		return [
			['**', '<strong></strong>'],
			['*hello*', '<strong>hello</strong>'],
			['*hello', '*hello'],
			['*hello* *world', '<strong>hello</strong> *world'],
			['hello', 'hello'],
			['hello*', 'hello*'],
			['foo *hello*', 'foo <strong>hello</strong>'],
			['foo *hello* bar', 'foo <strong>hello</strong> bar'],
			['*hello* bar', '<strong>hello</strong> bar'],
			['foo *hello* **bar**', 'foo <strong>hello</strong> <strong></strong>bar<strong></strong>'],
			['foo *hello* *bar*', 'foo <strong>hello</strong> <strong>bar</strong>'],
			['foo *HELLO* *bar*', 'foo <strong>HELLO</strong> <strong>bar</strong>'],
			['*Job Title: leader*', '<strong>Job Title: leader</strong>'],
		];
	}
}