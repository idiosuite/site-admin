<?php

namespace Tests\Admin\News;

use \App\Testing\AdminTestCase;
use \App\Domain\News;
use \App\Domain\Attachment;

class AttachmentTest extends AdminTestCase
{
	/** @test */
	public function it_displays_signin_page_if_not_signed_in()
	{
		$this
			->visit('NewAttachment')
			->seeSigninPage();
	}

	/** @test */
	public function it_displays_empty_form_for_news()
	{
		$news = $this->persist(News::class);

		$this
			->signedIn()
			->visitNewAttachment($news)
			->seeEmptyAttachmentForm($news);
	}

	/** @test */
	public function it_saves_a_file_attachment_for_a_news()
	{
		$news = $this->persist(News::class);
		$attachment = $this->create(Attachment::class);

		$this->bootstrapVfs();

		$this->assertFileAttachmentCount(0, $news);

		$this
			->signedIn()
			->visitNewAttachment($news)
			->fillAttachmentForm($attachment)
			->attachFile()
			->submit()
			->seeNewsPage();

		$this->assertFileAttachmentCount(1, $news);
	}

	/** @test */
	public function it_saves_a_link_attachment_for_a_news()
	{
		$news = $this->persist(News::class);
		$attachment = $this->create(Attachment::class);

		$this->bootstrapVfs();

		$this->assertLinkAttachmentCount(0, $news);

		$this
			->signedIn()
			->visitNewAttachment($news)
			->fillAttachmentForm($attachment)
			->attachLink('https://www.youtube.com/watch?v=a1Y73sPHKxw')
			->submit()
			->seeNewsPage();

		$this->assertLinkAttachmentCount(1, $news);
	}

	/** @test */
	public function it_displays_error_if_file_could_not_be_uploaded()
	{
		$news = $this->persist(News::class);
		$attachment = $this->create(Attachment::class);

		$this->bootstrapVfs();

		$this->assertFileAttachmentCount(0, $news);

		$largeFile = $this->createLargeFile();

		$this
			->signedIn()
			->visitNewAttachment($news)
			->fillAttachmentForm($attachment)
			->attachFile(['tmp_name' => $largeFile->url()])
			->submit()
			->seeAttachmentPage()
			->seeValidationError('attachment.attachment', 'Max allowed file size is 5242880 bytes')
			->seeValidationError('attachment.attachment', 'Only allowed mime types are');

		$this->assertFileAttachmentCount(0, $news);
	}

	/** @test */
	public function it_wont_blow_if_no_attachment_file_is_provided()
	{
		$news = $this->persist(News::class);

		$this
			->signedIn()
			->visitNewAttachment($news)
			->seeForm('attachmentForm')
				->seeInput('title')
					->enter('Foobar')
			->submit();

		$this->assertObjectCount(Attachment::class, 0);
		$this->assertCount(0, $news->attachments);
	}

	/** @test */
	public function it_validates_an_attachment()
	{
		$news = $this->persist(News::class);

		$this
			->signedIn()
			->visitNewAttachment($news)
			->seeForm('attachmentForm')
			->submit()
			->seeAttachmentPage()
			->seeErrorFeedback('Missing required fields')
			->seeValidationError('attachment.title')
			->seeValidationError('attachment.attachment');

		$this->assertObjectCount(Attachment::class, 0);
		$this->assertCount(0, $news->attachments);
	}

	/** @test */
	public function it_allows_to_unlink_an_attachment_from_a_news()
	{
		$news = $this->persist(News::class);
		$attachment = $this->persist(Attachment::class);
		$news->attachments()->attach($attachment);

		$this->assertCount(1, $news->attachments);

		$this
			->signedIn()
			->visit('EditNews', ['news_id' => $news->id])
			->clickUnlinkAttachment($attachment, $news)
			->seeForm('unlinkForm')
			->submit()
			->seeSuccessFeedback("L'allegato è stato rimosso")
			->dontSeeElement('div.attachment-' . $attachment->id);

		$this->assertCount(0, $news->attachments);
	}

	/** @test */
	public function it_wont_unlink_attachment_from_news_if_user_cancels()
	{
		$news = $this->persist(News::class);
		$attachment = $this->persist(Attachment::class);
		$news->attachments()->attach($attachment);

		$this->assertCount(1, $news->attachments);

		$this
			->signedIn()
			->visit('EditNews', ['news_id' => $news->id])
			->clickUnlinkAttachment($attachment, $news)
			->seeForm('unlinkForm')
			->click('Annulla')
			->seeElement('div.attachment-' . $attachment->id);

		$this->assertCount(1, $news->attachments);
	}

	private function visitNewAttachment($domainObject)
	{
		return $this->visit('NewAttachment', [$domainObject->getForeignKey() => $domainObject->id]);
	}

	private function clickUnlinkAttachment(Attachment $attachment, $domainObject)
	{
		return $this
			->click('Rimuovi');
	}

	private function seeEmptyAttachmentForm($domainObject)
	{
		return $this
			->seeForm('attachmentForm')
			->seeInput('title')
				->withValue('')
			->seeFile('attachment')
			->seeInput('url')
				->withValue('')
			->seeInput($domainObject->getForeignKey())
				->withValue($domainObject->id)
			->seeInput('id')
				->withValue('');
	}

	private function fillAttachmentForm(Attachment $attachment)
	{
		return $this
			->seeForm('attachmentForm')
			->seeInput('title')
				->enter($attachment->title);
	}

	private function attachFile(array $fileToUpload = [])
	{
		return $this
			->seeFile('attachment')
				->enter($this->getFileToUpload($fileToUpload));
	}

	private function attachLink($link)
	{
		return $this
			->seeInput('url')
				->enter($link);
	}

	private function getFileToUpload(array $override = [])
	{
		return array_merge([
			'name' => 'test.png',
			'type' => 'image/png',
			'tmp_name' => __DIR__ . '/../../data/test.png',
			'error' => UPLOAD_ERR_OK,
			'size' => '1024',
		], $override);
	}

	private function assertFileAttachmentCount($count, $news)
	{
		$this->assertUploadsCount($count);
		$this->assertObjectCount(Attachment::class, $count);
		$this->assertCount($count, $news->attachments);
	}

	private function assertLinkAttachmentCount($count, $news)
	{
		$this->assertUploadsCount(0);
		$this->assertObjectCount(Attachment::class, $count);
		$this->assertCount($count, $news->attachments);
	}

	private function seeNewsPage()
	{
		return $this->seeTitle('News');
	}

	private function seeAttachmentPage()
	{
		return $this->seeTitle('Allegato');
	}
}