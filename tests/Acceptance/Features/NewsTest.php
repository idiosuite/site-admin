<?php

namespace Tests\Admin\News;

use \App\Testing\AdminTestCase;
use \App\Domain\News;
use \App\Domain\Author;

class NewsTest extends AdminTestCase
{
	/** @test */
	public function it_displays_empty_form_for_a_new_item()
	{
		$this
			->signedIn()
			->visit('EditNews')
			->seeEmptyNewsForm();
	}

	/** @test */
	public function it_displays_empty_form_for_a_new_item_with_another_news_with_author_in_the_database()
	{
		$news = $this->persist(News::class, [
			'type' => 'news', 
			'subject' => 'Foo bar', 
			'body' => 'Lorem ipsum dolor',
			'display_date' => '2017-05-12 00:00:00',
		]);

		$john = $this->persist(Author::class, ['slug' => 'john-doe']);
		$jane = $this->persist(Author::class, ['slug' => 'jane-doe']);

		$news->authors()->attach($jane);

		$this
			->signedIn()
			->visit('EditNews')
			->seeEmptyNewsForm();
	}

	/** @test */
	public function it_saves_a_news_item_without_author()
	{
		$news = $this->create(News::class, [
			'type' => 'news', 
			'subject' => 'Foo bar', 
			'body' => 'Lorem ipsum dolor',
			'display_date' => '2017-05-12 00:00:00',
		]);

		$this
			->signedIn()
			->visit('EditNews')
			->fillNewsForm($news)
			->submit()
			->seeElement('div.card--news')
			->times(1);

		$this->assertObjectCount(News::class, 1);
		$this->assertObjectCount(Author::class, 0);

		$this->assertObjectMatches(News::class, 0, [
			'type' => 'news',
			'subject' => 'Foo bar',
			'body' => 'Lorem ipsum dolor',
			'display_date' => '2017-05-12 00:00:00',
			'published_at' => null,
		]);

		$this->assertHasNoAuthor((new News)->find()->getFirst());
	}

	/** @test */
	public function it_saves_a_news_item_with_author()
	{
		$john = $this->persist(Author::class, ['slug' => 'john-doe']);
		$jane = $this->persist(Author::class, ['slug' => 'jane-doe']);

		$news = $this->create(News::class, [
			'type' => 'news', 
			'subject' => 'Foo bar', 
			'body' => 'Lorem ipsum dolor',
			'display_date' => '2017-05-12 00:00:00',
		]);

		$this
			->signedIn()
			->visit('EditNews')
			->fillNewsForm($news, $john)
			->submit()
			->seeElement('div.card--news')
			->times(1);

		$this->assertObjectCount(News::class, 1);
		$this->assertObjectCount(Author::class, 2);

		$this->assertObjectMatches(News::class, 0, [
			'type' => 'news',
			'subject' => 'Foo bar',
			'body' => 'Lorem ipsum dolor',
			'display_date' => '2017-05-12 00:00:00',
			'published_at' => null,
		]);

		$this->assertAuthor($john, (new News)->find()->getFirst());
	}

	/** @test */
	public function it_displays_filled_form_for_existing_news_item()
	{
		$news = $this->persist(News::class, ['type' => 'news']);

		$this
			->signedIn()
			->visit('EditNews', ['news_id' => $news->id])
			->seeFilledNewsForm($news);
	}

	/** @test */
	public function it_displays_filled_form_for_existing_news_item_with_author()
	{
		$john = $this->persist(Author::class, ['slug' => 'john-doe']);
		$jane = $this->persist(Author::class, ['slug' => 'jane-doe']);

		$news = $this->persist(News::class, ['type' => 'news']);
		$news->authors()->attach($john);

		$this
			->signedIn()
			->visit('EditNews', ['news_id' => $news->id])
			->seeFilledNewsForm($news, $john);
	}

	/** @test */
	public function it_updates_an_existing_news_item()
	{
		$originalNews = $this->persist(News::class, ['type' => 'news']);

		$updatedNews = $this->create(News::class, [
			'type' => 'event', 
			'subject' => 'Hello', 
			'body' => 'World'
		]);

		$this
			->signedIn()
			->visit('EditNews', ['news_id' => $originalNews->id])
			->fillNewsForm($updatedNews)
			->submit();

		$this->assertObjectMatches(News::class, 0, [
			'type' => 'event',
			'subject' => 'Hello',
			'body' => 'World',
			'published_at' => null,
		]);

		$this->assertHasNoAuthor((new News)->find()->getFirst());
	}

	/** @test */
	public function it_updates_an_existing_news_item_author()
	{
		$john = $this->persist(Author::class, ['slug' => 'john-doe']);
		$jane = $this->persist(Author::class, ['slug' => 'jane-doe']);

		$originalNews = $this->persist(News::class, ['type' => 'news']);
		$originalNews->authors()->attach($john);
		
		$updatedNews = $this->create(News::class, [
			'type' => 'event', 
			'subject' => 'Hello', 
			'body' => 'World'
		]);

		$this->assertAuthor(
			$john, 
			(new News)->find()->getFirst()
		);

		$this
			->signedIn()
			->visit('EditNews', ['news_id' => $originalNews->id])
			->fillNewsForm($updatedNews, $jane)
			->submit();

		$this->assertObjectMatches(News::class, 0, [
			'type' => 'event',
			'subject' => 'Hello',
			'body' => 'World',
			'published_at' => null,
		]);

		$this->assertAuthor(
			$jane, 
			(new News)->find()->getFirst()
		);
	}

	/** @test */
	public function it_updates_a_news_item_with_author_when_author_is_removed()
	{
		$john = $this->persist(Author::class, ['slug' => 'john-doe']);
		$jane = $this->persist(Author::class, ['slug' => 'jane-doe']);
		$noAuthor = new Author;

		$originalNews = $this->persist(News::class, ['type' => 'news']);
		$originalNews->authors()->attach($john);
		
		$updatedNews = $this->create(News::class, [
			'type' => 'event', 
			'subject' => 'Hello', 
			'body' => 'World'
		]);

		$this->assertAuthor($john, $originalNews);

		$this
			->signedIn()
			->visit('EditNews', ['news_id' => $originalNews->id])
			->fillNewsForm($updatedNews, $noAuthor)
			->submit();

		$this->assertObjectMatches(News::class, 0, [
			'type' => 'event',
			'subject' => 'Hello',
			'body' => 'World',
			'published_at' => null,
		]);

		$this->assertHasNoAuthor((new News)->find()->getFirst());
	}

	/** @test */
	public function it_will_trash_a_news_through_a_confirmation_screen()
	{
		// Assuming I have a news
		$news = $this->persist(News::class);

		$this->assertObjectCount(News::class, 1);

		// And I list the news
		$this->signedIn()
			->visit('ListNews')
			->attemptToTrash($news)
			->seeSuccessFeedback('La news è stata eliminata')
			->seeTitle('News');
		
		// Then the news is trashed
		$this->assertAvailableObjectCount(News::class, 0);
		$this->assertTrashedObjectCount(News::class, 1);
	}

	/** @test */
	public function it_wont_trash_a_news_if_user_clicks_cancel()
	{
		// Assuming I have a news
		$news = $this->persist(News::class);

		$this->assertObjectCount(News::class, 1);

		// And I list the news
		$this->signedIn()
			->visit('ListNews')
			->attemptToTrashButCancel($news)
			->seeTitle('News');
		
		// Then the news is trashed
		$this->assertAvailableObjectCount(News::class, 1);
		$this->assertTrashedObjectCount(News::class, 0);
	}

	/** @test */
	public function it_wont_trash_a_news_via_get_request()
	{
		// Assuming I have a news
		$news = $this->persist(News::class);

		$this->assertObjectCount(News::class, 1);

		// And I list the news
		$this
			->signedIn()
			->visit('TrashNews', ['news_id' => $news->id]);
		
		// Then the news is trashed
		$this->assertAvailableObjectCount(News::class, 1);
		$this->assertTrashedObjectCount(News::class, 0);
	}

	/** @test */
	public function it_validates_news()
	{
		$this
			->signedIn()
			->visit('EditNews')
			->seeForm('newsForm')
				->seeSelect('type')
					->choose('foobar')
			->submit()
			->seeErrorFeedback('Missing required fields')
			->seeValidationError('news.type')
			->seeValidationError('news.subject')
			->seeValidationError('news.body');
	}

	/** @test */
	public function it_wont_save_a_news_without_valid_token()
	{
		$aNews = $this->create(News::class);

		$this
			->signedIn()
			->visit('EditNews')
			->fillNewsForm($aNews)
			->submitWithoutToken()
			->seeInvalidRequestPage();

		// And the news should not be saved
		$this->assertObjectCount(News::class, 0);
	}

	private function fillNewsForm(News $news, Author $author = null)
	{
		return $this->seeForm('newsForm')
			->seeSelect('type')
				->enter($news->type)
			->seeSelect('author')
				->enter($author ? $author->slug : '')
			->seeInput('subject')
				->enter($news->subject)
			->seeTextarea('body')
				->enter($news->body)
			->seeSelect('publishDate_dd')
				->enter($news->getDisplayDateDay())
			->seeSelect('publishDate_mm')
				->enter($news->getDisplayDateMonth())
			->seeSelect('publishDate_yy')
				->enter($news->getDisplayDateYear());
	}

	private function seeEmptyNewsForm()
	{
		return $this->seeForm('newsForm')
			->seeSelect('type')
				->withOptions(['news', 'event'])
				->withValue('news')
			->seeSelect('author')
				->withOptions($this->getAvailableAuthorsSlugs())
				->withValue('')
			->seeInput('subject')
				->withValue('')
			->seeTextarea('body')
				->withValue('')
			->seeSelect('publishDate_dd')
				->withValue('')
			->seeSelect('publishDate_mm')
				->withValue('')
			->seeSelect('publishDate_yy')
				->withValue('')
			->seeCheckbox('published')
				->withValue(false)
			->seeInput('news_id')
				->withValue('');
	}

	 private function seeFilledNewsForm(News $news, Author $author = null)
	{
		$author = $author ?: new Author;

		return $this->seeForm('newsForm')
			->seeSelect('type')
				->withOptions(['news', 'event'])
				->withValue($news->type)
			->seeSelect('author')
				->withOptions($this->getAvailableAuthorsSlugs())
				->withValue($author->slug)
			->seeInput('subject')
				->withValue($news->subject)
			->seeTextarea('body')
				->withValue($news->body)
			->seeSelect('publishDate_dd')
				->withValue($news->getDisplayDateDay())
			->seeSelect('publishDate_mm')
				->withValue($news->getDisplayDateMonth())
			->seeSelect('publishDate_yy')
				->withValue($news->getDisplayDateYear())
			->seeCheckbox('published')
				->withValue(false)
			->seeInput('news_id')
				->withValue($news->id);
	}

	private function getAvailableAuthorsSlugs()
	{
		return array_map(
			function ($author) {
				return $author['slug'];
			}, 
			(new Author)->find()->toArray()
		);
	}

	private function assertAuthor(Author $expectedAuthor, News $news)
	{
		$authors = $news->authors;
		$this->assertNotNull($authors);
		$this->assertCount(1, $authors);
		$this->assertEquals($expectedAuthor->slug, $authors->getFirst()->slug);
	}

	private function assertHasNoAuthor(News $news)
	{
		$this->assertCount(0, $news->authors);
	}
}