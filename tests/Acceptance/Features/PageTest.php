<?php

namespace Tests\Admin;

use \App\Testing\AdminTestCase;
use \App\Domain\Page;

class PageTest extends AdminTestCase
{
    /** @test */
	public function it_displays_empty_form_for_a_new_item()
	{
		$this
            ->signedIn()
            ->visit('EditPage')
            ->seeEmptyPageForm();
	}

	/** @test */
	public function it_saves_a_new_page()
	{
		$aPage = $this->create(Page::class, [
			'slug' => 'alessandro-piona',
			'title' => 'Dott. Alessandro Piona',
			'body' => 'Lorem ipsum dolor sit amet',
		]);

		$this->assertObjectCount(Page::class, 0);

		$this
			->signedIn()
			->visit('EditPage')
			->fillPageForm($aPage)
			->submit();

		$this->assertObjectCount(Page::class, 1);
		$this->assertObjectMatches(Page::class, 0, [
			'slug' => 'alessandro-piona',
			'title' => 'Dott. Alessandro Piona',
			'body' => 'Lorem ipsum dolor sit amet',
			'created_at' => gmdate('Y-m-d H:i:s'),
			'published_at' => gmdate('Y-m-d H:i:s'),
		]);
	}

	/** @test */
	public function it_displays_filled_form_for_existing_page()
	{
		$aPage = $this->persist(Page::class, [
			'slug' => 'alessandro-piona',
			'title' => 'Dott. Alessandro Piona',
			'body' => 'Lorem ipsum dolor sit amet',
			'published_at' => gmdate('Y-m-d H:i:s')
		]);

		$this
			->signedIn()
			->visit('EditPage', ['page_id' => $aPage->id])
			->seeFilledPageForm($aPage);
	}

	/** @test */
	public function it_trashes_a_page_through_a_confirmation_screen()
	{
		$page = $this->persist(Page::class);

		$this->assertObjectCount(Page::class, 1);

		$this
			->signedIn()
			->visit('ListPage')
			->attemptToTrash($page)
			->seeSuccessFeedback('La pagina è stata eliminata')
			->seeTitle('Pagine');;

		$this->assertAvailableObjectCount(Page::class, 0);
        $this->assertTrashedObjectCount(Page::class, 1);
	}

	/** @test */
	public function it_wont_trash_a_page_if_user_cancels()
	{
		$page = $this->persist(Page::class);

		$this->assertObjectCount(Page::class, 1);

		$this
			->signedIn()
			->visit('ListPage')
			->attemptToTrashButCancel($page)
			->seeTitle('Pagine');;

		$this->assertAvailableObjectCount(Page::class, 1);
        $this->assertTrashedObjectCount(Page::class, 0);
	}

	/** @test */
	public function it_wont_trash_a_page_with_get_request()
	{
		$aPage = $this->persist(Page::class);

		$this->assertObjectCount(Page::class, 1);

		$this
			->signedIn()
			->visit('TrashPage', ['page_id' => $aPage->id]);

		$this->assertAvailableObjectCount(Page::class, 1);
        $this->assertTrashedObjectCount(Page::class, 0);
	}

	/** @test */
	public function it_wont_save_a_page_without_valid_token()
	{
		$aPage = $this->create(Page::class);

		$this
			->signedIn()
			->visit('EditPage')
			->fillPageForm($aPage)
			->submitWithoutToken()
			->seeInvalidRequestPage();

		$this->assertObjectCount(Page::class, 0);
	}

	/** @test */
	public function it_validates_form_submission()
	{
		$this
			->signedIn()
			->visit('EditPage')
			->seeForm('pageForm')
				->submit()
			->seeErrorFeedback('Missing required fields')
			->seeValidationError('page.slug')
			->seeValidationError('page.title')
			->seeValidationError('page.body');
	}

	private function seeEmptyPageForm()
	{
		return $this
			->seeForm('pageForm')
			->seeInput('slug')
				->withValue('')
			->seeInput('title')
				->withValue('')
			->seeTextarea('body')
				->withValue('')
			->seeCheckbox('published')
				->withValue('');
	}

	private function fillPageForm(Page $page)
	{
		return $this
			->seeForm('pageForm')
			->seeInput('slug')
				->enter($page->slug)
			->seeInput('title')
				->enter($page->title)
			->seeTextarea('body')
				->enter($page->body)
			->seeCheckbox('published')
				->check();
	}

	private function seeFilledPageForm(Page $page)
	{
		return $this
			->seeForm('pageForm')
			->seeInput('slug')
				->withValue($page->slug)
			->seeInput('title')
				->withValue($page->title)
			->seeTextarea('body')
				->withValue($page->body)
			->seeCheckbox('published')
				->withValue(true);
	}
}