<?php

namespace App\Domain;

use \PHPUnit\Framework\TestCase;

use \App\Validation\LanguageFilter;

class FilterableTest extends TestCase
{
	/** @test */
	public function it_filters_a_property_if_a_filter_is_provided()
	{
		$page = new Page;

		$page->setFilter('foo', LanguageFilter::class);
		
		$page->setProperty('foo', 'it_IT');
		$this->assertEquals('it_IT', $page->getProperty('foo'));
		
		$page->setProperty('foo', 'foobar');
		$this->assertNull($page->getProperty('foo'));
	}

	/** @test */
	public function it_filters_multiple_properties_if_a_filter_is_provided()
	{
		$page = new Page;

		$page->setFilter('foo', LanguageFilter::class);
		
		$page->setProperties(['foo' => 'it_IT']);
		$this->assertEquals('it_IT', $page->getProperty('foo'));
		
		$page->setProperties(['foo' => 'foobar']);
		$this->assertNull($page->getProperty('foo'));
	}

	/** @test */
	public function it_registers_a_filter()
	{
		$page = new Page;
		
		$page->setProperty('foo', 'bar');
		$this->assertEquals('bar', $page->getProperty('foo'));
		
		$page->setFilter('foo', new LanguageFilter);
		
		$page->setProperty('foo', 'bar');
		$this->assertNull($page->getProperty('foo'));
		
		$page->setProperty('foo', 'it_IT');
		$this->assertEquals('it_IT', $page->getProperty('foo'));
	}

	/** @test */
	public function it_allows_to_register_multiple_filters()
	{
		$page = new Page;

		$fooFilter = new \App\Validation\WhitelistFilter(['foo']);
		$barFilter = new \App\Validation\WhitelistFilter(['foo', 'bar']);

		$page->setFilter('foo', $fooFilter);
		$page->setFilter('foo', $barFilter);

		$page->setProperty('foo', 'foo');
		$this->assertEquals('foo', $page->getProperty('foo'));
		
		$page->setProperty('foo', 'bar');
		$this->assertNull($page->getProperty('foo'));
		
		$page->setProperty('foo', 'baz');
		$this->assertNull($page->getProperty('baz'));
	}
}