<?php

namespace Tests\Acceptance\App\Repository;

use \PHPUnit\Framework\TestCase;
use \Prophecy\PhpUnit\ProphecyTrait;

use \App\Repository\Record;
use \App\Repository\TableGateway;
use \App\Repository\Map;
use \App\Repository\ArrayAssignments;

class RecordTest extends TestCase
{
    use ProphecyTrait;
    
	private $map;
    private $tableGateway;

    protected function setUp(): void
    {
        $this->map = new Map;
        $this->tableGateway = $this->prophesize(TableGateway::class);
    }

    /** @test */
    public function it_saves_a_new_assignments()
    {
    	$assignments = new ArrayAssignments([
            'body' => 'Lorem ipsum dolor sit amet'
        ]);

        $recordable = new Record($this->map, $this->tableGateway->reveal(), $assignments);

    	$this->tableGateway->insert([
    		'body' => 'Lorem ipsum dolor sit amet', 
    		'created_at' => gmdate('Y-m-d H:i:s')
    	])->shouldBeCalled();

    	$this->tableGateway->lastInsertId()->willReturn(1);

    	$recordable->save();

    	$this->assertEquals(1, $assignments->getAssignment('id'));
    }

    /** @test */
    public function it_updates_a_existing_assignments()
    {
    	$assignments = new ArrayAssignments([
            'id' => 1, 
            'body' => 'Lorem ipsum dolor sit amet'
        ]);

        $recordable = new Record($this->map, $this->tableGateway->reveal(), $assignments);

    	$this->tableGateway->update([
    		'body' => 'Lorem ipsum dolor sit amet', 
    		'updated_at' => gmdate('Y-m-d H:i:s')
    	], ['id' => 1])->shouldBeCalled();

    	$recordable->save();
    }

    /** @test */
    public function it_trashes_a_existing_assignments()
    {
    	$assignments = new ArrayAssignments([
            'id' => 1, 
            'body' => 'Lorem ipsum dolor sit amet'
        ]);

        $recordable = new Record($this->map, $this->tableGateway->reveal(), $assignments);

    	$this->tableGateway->update([
    		'deleted_at' => gmdate('Y-m-d H:i:s')
    	], ['id' => 1])->shouldBeCalled();

    	$recordable->trash();
    }

    /** @test */
    public function it_wont_trash_a_new_assignments()
    {
    	$assignments = new ArrayAssignments([
            'body' => 'Lorem ipsum dolor sit amet'
        ]);

        $recordable = new Record($this->map, $this->tableGateway->reveal(), $assignments);

    	$this->tableGateway->update($this->any(), $this->any())->shouldNotBeCalled();

    	$recordable->trash();
    }

    /** @test */
    public function it_restores_a_trashed_assignments()
    {
    	$assignments = new ArrayAssignments([
    		'id' => 1,
    		'body' => 'Lorem ipsum dolor sit amet', 
    		'deleted_at' => gmdate('Y-m-d H:i:s')
    	]);

        $recordable = new Record($this->map, $this->tableGateway->reveal(), $assignments);

    	$this->tableGateway->update(['deleted_at' => null], ['id' => 1])->shouldBeCalled();

    	$recordable->restore();
    }

    /** @test */
    public function it_wont_restore_existing_assignments()
    {
    	$assignments = new ArrayAssignments([
    		'id' => 1,
    		'body' => 'Lorem ipsum dolor sit amet', 
    		'deleted_at' => null
    	]);

        $recordable = new Record($this->map, $this->tableGateway->reveal(), $assignments);

    	$this->tableGateway->update(['deleted_at' => null], ['id' => 1])->shouldNotBeCalled();

    	$recordable->restore();
    }

    /** @test */
    public function it_wont_restore_new_assignments()
    {
    	$assignments = new ArrayAssignments([
    		'body' => 'Lorem ipsum dolor sit amet'
    	]);

        $recordable = new Record($this->map, $this->tableGateway->reveal(), $assignments);

    	$this->tableGateway->update($this->any(), $this->any())->shouldNotBeCalled();

    	$recordable->restore();
    }

    /** @test */
    public function it_deletes_existing_assignments()
    {
    	$assignments = new ArrayAssignments([
    		'id' => 1,
    		'body' => 'Lorem ipsum dolor sit amet', 
    	]);

        $recordable = new Record($this->map, $this->tableGateway->reveal(), $assignments);

    	$this->tableGateway->delete(['id' => 1])->shouldBeCalled();

    	$recordable->delete();
    }

    /** @test */
    public function it_wont_delete_new_assignments()
    {
    	$assignments = new ArrayAssignments([
    		'body' => 'Lorem ipsum dolor sit amet', 
    	]);

        $recordable = new Record($this->map, $this->tableGateway->reveal(), $assignments);

    	$this->tableGateway->delete($this->any())->shouldNotBeCalled();

    	$recordable->delete();
    }

    /** @test */
    public function it_wont_delete_a_locked_assignments()
    {
    	$assignments = new ArrayAssignments([
    		'id' => 1,
    		'body' => 'Lorem ipsum dolor sit amet', 
    	]);

    	$this->map->setIsDeletable(false);

        $recordable = new Record($this->map, $this->tableGateway->reveal(), $assignments);

    	$this->tableGateway->delete(['id' => 1])->shouldNotBeCalled();

    	$recordable->delete();
    }
}