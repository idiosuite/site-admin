<?php

namespace Tests\Acceptance\App\Repository;

use \PHPUnit\Framework\TestCase;

use \App\Testing\AdminTestCase;

use \App\Repository\Repository;

use \App\Repository\Map;
use \App\Database\TableGatewayFactory;
use \App\Domain\News;
use \App\Domain\Collection;

class RepositoryTest extends AdminTestCase
{
	/** @before */
	protected function setUpRepository()
	{
		$this->newsCollection = new Collection([], new News, new News);
		$this->table = TableGatewayFactory::create('news');
		$this->map = new Map;

		$this->repository = new Repository($this->map, $this->table, $this->newsCollection);
	}
	
	/** @after */
	protected function tearDownRepository()
	{
		TableGatewayFactory::reset();
	}

	/** @test */
	public function it_finds_a_domain_object_by_id()
	{
		$this->insert([
			['subject' => 'Foobar', 'body' => 'Lorem ipsum dolor'],
			['subject' => 'Foobar', 'body' => 'Sit amet consectetur'],
			['subject' => 'Foobar', 'body' => 'Adipiscing elit'],
		]);
		
		$news = $this->repository->findById(2);

		$this->assertInstanceOf(News::class, $news);
		$this->assertEquals(2, $news->id);
		$this->assertEquals('Sit amet consectetur', $news->body);
	}

	/** @test */
	public function it_finds_all_domain_objects()
	{
		$this->insert([
			['subject' => 'Foobar', 'body' => 'Lorem ipsum dolor'],
			['subject' => 'Foobar', 'body' => 'Sit amet consectetur'],
			['subject' => 'Foobar', 'body' => 'Adipiscing elit'],
		]);

		$collection = $this->repository->find();

		$this->assertInstanceOf(Collection::class, $collection);
		$this->assertCount(3, $collection);
	}

	/** @test */
	public function it_allows_to_create_a_dynamic_query()
	{
		$this->insert([
			['subject' => 'Foobar', 'body' => 'Lorem ipsum dolor'],
			['subject' => 'Foobar', 'body' => 'Sit amet consectetur'],
			['subject' => 'Foobar', 'body' => 'Adipiscing elit'],
		]);
		
		$collection = $this->repository->where([['body', 'eq', 'Adipiscing elit']])->find();

		$this->assertInstanceOf(Collection::class, $collection);
		$this->assertCount(1, $collection);
	}

	/** @test */
	public function it_allows_to_register_a_global_scope()
	{
		$this->insert([
			['subject' => 'Foo', 'body' => 'Lorem ipsum dolor'],
			['subject' => 'Bar', 'body' => 'Sit amet consectetur'],
			['subject' => 'Baz', 'body' => 'Adipiscing elit'],
		]);
		
		$scope = new \App\Repository\Scope\ScopeStub(['subject' => 'foo']);
		
		$this->repository->registerGlobalScope($scope);

		$collection = $this->repository->find();

		$this->assertInstanceOf(Collection::class, $collection);
		$this->assertCount(1, $collection);
	}

	private function insert(array $rows)
	{
		foreach ($rows as $row) {
			$this->table->insert($row);
		}
	}
}