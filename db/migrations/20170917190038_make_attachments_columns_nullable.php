<?php

use Phinx\Migration\AbstractMigration;

class MakeAttachmentsColumnsNullable extends AbstractMigration
{
    public function up()
    {
        $table = $this->table('attachments');
        
        $table->changeColumn('filename', 'string', ['null' => true]);
        $table->changeColumn('size', 'integer', ['null' => true]);
        $table->changeColumn('mimetype', 'string', ['null' => true]);
        
        $table->save();
    }

    public function down()
    {

    }
}
