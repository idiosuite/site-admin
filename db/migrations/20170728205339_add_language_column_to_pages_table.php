<?php

use Phinx\Migration\AbstractMigration;

class AddLanguageColumnToPagesTable extends AbstractMigration
{
    public function change()
    {
        $table = $this->table('pages');

        $table->addColumn('language', 'string', ['length' => 5, 'default' => 'it_IT', 'after' => 'id']);

        $table->update();
    }
}
