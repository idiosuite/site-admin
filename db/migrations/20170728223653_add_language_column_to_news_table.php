<?php

use Phinx\Migration\AbstractMigration;

class AddLanguageColumnToNewsTable extends AbstractMigration
{
    public function change()
    {
        $table = $this->table('news');

        $table->addColumn('language', 'string', ['length' => 5, 'default' => 'it_IT', 'after' => 'id']);

        $table->update();
    }
}
