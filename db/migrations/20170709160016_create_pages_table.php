<?php

use Phinx\Migration\AbstractMigration;

class CreatePagesTable extends AbstractMigration
{
    public function change()
    {
        $table = $this->table('pages');

        $table
            ->addColumn('slug', 'string')
            ->addColumn('heroImage', 'string', ['null' => true])
            ->addColumn('title', 'string')
            ->addColumn('body', 'text');

        $table->addTimestamps();
        $table->addColumn('deleted_at', 'timestamp', ['null' => true]);
        $table->addColumn('published_at', 'timestamp', ['null' => true]);

        $table->addIndex(['slug'], ['unique' => true, 'name' => 'slug']);

        $table->create();
    }
}
