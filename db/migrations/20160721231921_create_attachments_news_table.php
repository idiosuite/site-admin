<?php

use Phinx\Migration\AbstractMigration;

class CreateAttachmentsNewsTable extends AbstractMigration
{
    public function change()
    {
        $table = $this->table('attachments_news', ['id' => false, 'timestamps' => false]);

        $table
            ->addColumn('news_id', 'integer')
            ->addColumn('attachment_id', 'integer');

        $table->create();
    }
}
