<?php

use Phinx\Migration\AbstractMigration;

class ChangePagesTableIndexAddingLanguageConstraint extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $table = $this->table('pages');
        $table->removeIndexByName('slug');

        $table->addIndex(['slug', 'language'], ['unique' => true, 'name' => 'slug']);
        $table->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $table = $this->table('pages');
        $table->addIndex(['slug'], ['unique' => true, 'name' => 'slug']);
        $table->update();
    }
}
