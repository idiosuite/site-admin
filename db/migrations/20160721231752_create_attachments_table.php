<?php

use Phinx\Migration\AbstractMigration;

class CreateAttachmentsTable extends AbstractMigration
{
    public function change()
    {
        $table = $this->table('attachments');

        $table
            ->addColumn('title', 'string')
            ->addColumn('permalink', 'text')
            ->addColumn('filename', 'string')
            ->addColumn('size', 'integer')
            ->addColumn('mimetype', 'string');

        $table->addTimestamps();
        $table->addColumn('deleted_at', 'timestamp', ['null' => true]);

        $table->create();
    }
}
