<?php

use Phinx\Migration\AbstractMigration;

class CreateNewsTable extends AbstractMigration
{
    public function change()
    {
        $table = $this->table('news');

        $table
        	->addColumn('subject', 'string')
            ->addColumn('body', 'text');

        $table->addTimestamps();
        $table->addColumn('deleted_at', 'timestamp', ['null' => true]);
        $table->addColumn('published_at', 'timestamp', ['null' => true]);

        $table->create();
    }
}
