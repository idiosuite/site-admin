<?php

use Phinx\Migration\AbstractMigration;

class AddTypeFlagToNewsTable extends AbstractMigration
{
    public function change()
    {
        $table = $this->table('news');

        $table->addColumn('type', 'string', ['default' => 'news']);

        $table->update();
    }
}
