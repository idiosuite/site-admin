<?php

require __DIR__ . '/vendor/autoload.php';

require __DIR__ . '/config.php';

use \App\Base\Environment;
use \App\Base\Config;

// Register environment settings
Environment::instance()->registerEnvironment('test', ['test']);
Environment::instance()->registerEnvironment('dev', ['local', 'site-admin.test']);
Environment::instance()->registerValue($_SERVER['HTTP_HOST']);

// Register config file
Config::registerDefaultConfig(__DIR__ . '/config/config.php');
Config::registerEnvConfig('dev', __DIR__ . '/config/config.dev.php');
Config::registerEnvConfig('test', __DIR__ . '/config/config.test.php');
Config::setEnvironment(Environment::instance()->env(null));

function config() {
	return Config::instance();
}