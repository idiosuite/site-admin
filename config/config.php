<?php

return [
	'baseHref' => 'https://' . $_SERVER['HTTP_HOST'],
	'routerBaseHref' => '/',
	'uploads' => [
		'basePath' => '',
		'baseUrl' => '',
		'allowedMaxSize' => 1024 * 5, // 5mb
		'allowedMimeTypes' => ['image/png', 'image/jpeg', 'application/pdf'],
	],
	'language' => [
		'available' => ['it_IT' => 'Italiano'],
		'default' => 'it_IT',
	],
];