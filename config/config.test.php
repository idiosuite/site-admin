<?php

return [
	'uploads' => [
		'basePath' => 'vfs://root/uploads',
		'baseUrl' => 'http://www.example.com/uploads',
	],
];