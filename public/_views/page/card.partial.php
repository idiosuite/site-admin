<?php

use \App\View\ViewHelper;

?>
<div class="card page-<?= $page->id; ?>">
	<header class="card-header">
		<p class="card-header-title"><?= $title; ?></p>
	</header>
	<div class="card-content">
		<div class="content">
			<p>
				<strong>Data: </strong><?= $formattedDate; ?><br/>
				<strong>Stato: </strong><?= $status; ?><br/>
				<strong>Lingua: </strong><?= $language; ?><br/>
			</p>
		</div>
	</div>
	<footer class="card-footer">
		<?= ViewHelper::editButton($page, ['class' => 'card-footer-item']); ?>
	    <?= ViewHelper::trashButton($page, ['class' => 'card-footer-item']); ?>
	</footer>
</div>
<hr/>