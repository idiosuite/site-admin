<?php
use \App\View\ViewHelper;
$page = ViewHelper::requestObject('page');
?>
<?php 
ViewHelper::view('default.trash', [
	'title' => 'Elimina pagina',
	'message' => sprintf("Sei sicuro di voler eliminare la pagina '%s'?", $page->title),
	'domainObject' => $page,
	'cancelCommand' => 'ListPage',
]); 
?>