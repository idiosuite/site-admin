<?php
use \App\View\ViewHelper;
?>
<section class="section">
	<div class="container">
		<h2 class="title">Pagina</h2>
		<?= ViewHelper::feedback(); ?>
		<form method="post" action="index.php?cmd=SavePage" enctype="multipart/form-data" class="Form" id="pageForm">
			<!-- Slug -->
			<div class="field">
				<label class="label" for="slug">Slug</label>
				<div class="control">
					<input class="input" type="text" name="slug" id="slug" value="<?= ViewHelper::old('page.slug'); ?>" />
				</div>
				<?= ViewHelper::validation('page.slug'); ?>
			</div>
			<!-- Title -->
			<div class="field">
				<label class="label" for="title">Titolo</label>
				<div class="control">
					<input class="input" type="text" name="title" id="title" value="<?= ViewHelper::old('page.title'); ?>" />
				</div>
				<?= ViewHelper::validation('page.title'); ?>
			</div>
			<!-- Content -->
			<div class="field">
				<label class="label" for="body">Contenuto</label>
				<div class="control">
					<textarea class="textarea" name="body" id="body" cols="90" rows="9"><?= ViewHelper::old('page.body'); ?></textarea>
				</div>
				<?= ViewHelper::validation('page.body'); ?>
			</div>
			<!-- Status -->
			<?php
			$checked = ViewHelper::old('page.published_at') ? 'checked' : '';
			?>
			<div class="field">
				<label for="published" class="checkbox">
					<input type="checkbox" name="published" id="published" value="1" <?= $checked ?> />
					Pubblicato
				</label>
			</div>
			<!-- Actions -->
			<div class="field is-grouped">
				<div class="control">
					<input type="submit" value="Salva" class="button is-primary is-large" />
				</div>
				<div class="control">
					<a href="index.php?cmd=ListPage" class="button is-secondary is-large">Annulla</a>
				</div>
			</div>
			<?= ViewHelper::csrfInput(); ?>
			<?= ViewHelper::methodInput('POST'); ?>
			<input type="hidden" name="page_id" value="<?= ViewHelper::old('page.id'); ?>" />
		</form>
	</div>
</section>