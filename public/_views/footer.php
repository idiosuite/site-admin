	<footer class="footer">
		<div class="container">
			<div class="content has-text-centered">
				<p>Copyright &copy; <?= gmdate('Y'); ?> <a href="http://www.molnapps.com">MolnApps</a></p>
			</div>
		</div>
	</footer>
</body>
</html>