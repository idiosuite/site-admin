<?php
use \App\View\ViewHelper;
$cancelUrl = 'index.php?cmd=' . $cancelCommand . '&' . $relative->getForeignKey() . '=' . $relative->id;
?>
<section class="hero is-warning">
	<div class="hero-body">
		<div class="container">
			<h2 class="title"><?= $title; ?></h2>
			<div class="content">
				<p><?= $message; ?></p>
			</div>
			<div class="field is-grouped">
				<div class="control">
					<?= ViewHelper::unlinkConfirmButton($domainObject, $relative, $unlinkCommand); ?>
				</div>
				<div class="control">
					<a href="<?= $cancelUrl ?>" class="button is-warning is-inverted is-outlined">Annulla</a>
				</div>
			</div>
		</div>
	</div>
</section>
