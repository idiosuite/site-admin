<?php
use \App\View\ViewHelper;
$news = ViewHelper::requestObject('news');
?>
<?php 
ViewHelper::view('default.trash', [
	'title' => 'Elimina news',
	'message' => sprintf("Sei sicuro di voler eliminare la news '%s'?", $news->subject),
	'domainObject' => $news,
	'cancelCommand' => 'ListNews',
]); 
?>