<?php
use \App\View\ViewHelper;
$trashButton = ViewHelper::unlinkButton(
	$attachment, 
	$news, 
	'ConfirmUnlinkAttachmentFromNews', 
	['class' => 'button is-danger']
);
?>
<div class="card attachment-<?= $attachment->id; ?>">
	<div class="card-content">
		<div class="level">
			<div class="level-left"><?= $attachment->title; ?></div>
			<div class="level-right">
				<p class="level-item"><a href="<?= $attachment->permalink; ?>" class="button is-light">Visualizza</a></p>
				<p class="level-item"><?= $trashButton; ?></p>
			</div>
		</div>
	</div>
</div>