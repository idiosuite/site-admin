<?php
use \App\View\ViewHelper;
?>
<hr/>
<section class="section">
	<div class="container">
		<h3 class="title is-4">Allegati</h3>
		<div class="level">
			<div class="level-left">
				<p class="level-item">
					<a class="button is-success" href="index.php?cmd=NewAttachment&news_id=<?= ViewHelper::old('news.id'); ?>">Nuovo allegato</a>
				</p>
			</div>
		</div>
		<?php
		$news = ViewHelper::requestObject('news');
		foreach ($news->attachments as $attachment) {
			include ('attachment.card.partial.php');
		}
		?>
	</div>
</section>