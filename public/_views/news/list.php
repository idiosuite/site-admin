<?php

use \App\View\ViewHelper;
use \Carbon\Carbon;
use \App\Base\AppContainer;

$newsCollection = ViewHelper::requestObject('newsCollection');
?>
<section class="section">
	<div class="container">
		<?= ViewHelper::feedback(); ?>
		<h2 class="title">News</h2>
		<nav class="level">
			<!-- Left side -->
			<div class="level-left">
				<p class="level-item"><a class="button is-success" href="index.php?cmd=NewNews">Nuova news</a></p>
			</div>
		</nav>
		<?php
		foreach ($newsCollection as $news) {
			extract([
				'subject' => $news->subject,
				'excerpt' => $news->getExcerpt(),
				'formattedDate' => Carbon::parse($news->created_at)->formatLocalized('%d %B %Y'),
				'attachmentsCount' => $news->attachments->count(),
				'status' => ($news->isPublished() ? 'Pubblicato' : 'Bozza'),
				'language' => AppContainer::get('languageProvider')->getLanguageName($news->language),
			]);
			include('card.partial.php');
		}
		?>
	</div>
</section>