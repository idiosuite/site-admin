<?php
use \App\View\ViewHelper;
use \Carbon\Carbon;
?>
<section class="section">
	<div class="container">
		<h2 class="title">News &amp; Eventi</h2>
		<?= ViewHelper::feedback(); ?>
		<form method="post" action="index.php?cmd=SaveNews" enctype="multipart/form-data" class="Form" id="newsForm">
			<!-- Type -->
			<div class="field">
				<label class="label" for="type">Tipo</label>
				<div class="control">
					<div class="select">
						<select name="type" id="type">
							<?= ViewHelper::options(['news' => 'News', 'event' => 'Evento'], ViewHelper::old('news.type')); ?>
						</select>
					</div>
				</div>
				<?= ViewHelper::validation('news.type'); ?>
			</div>
			<!-- Author -->
			<div class="field">
				<label class="label" for="author">Autore</label>
				<div class="control">
					<div class="select">
						<select name="author" id="author">
							<?= 
							ViewHelper::options(
								ViewHelper::availableAuthors(), 
								ViewHelper::requestObject('news')->getAuthor()->slug
							); 
							?>
						</select>
					</div>
				</div>
				<?= ViewHelper::validation('news.type'); ?>
			</div>
			<!-- Subject -->
			<div class="field">
				<label class="label" for="subject">Oggetto</label>
				<div class="control">
					<input class="input" type="text" name="subject" id="subject" value="<?= ViewHelper::old('news.subject'); ?>" />
				</div>
				<?= ViewHelper::validation('news.subject'); ?>
			</div>
			<!-- Message -->
			<div class="field">
				<label class="label" for="body">Messaggio</label>
				<div class="control">
					<textarea class="textarea" name="body" id="body" cols="90" rows="9"><?= ViewHelper::old('news.body'); ?></textarea>
				</div>
				<?= ViewHelper::validation('news.body'); ?>
			</div>
			<!-- Publish Date -->
			<?php 
			$label = 'Mostra data';
			$displayDate = ViewHelper::old('news.display_date');
			$date = Carbon::createFromFormat('Y-m-d H:i:s', $displayDate ?: gmdate('Y-m-d H:i:s'));
			$dd = $displayDate ? $date->format('j') : '';
			$mm = $displayDate ? $date->format('n') : '';
			$yy = $displayDate ? $date->format('Y') : '';
			include(__DIR__ . '/../_form/calendar.partial.php'); 
			?>
			<!-- Status -->
			<?php
			$checked = ViewHelper::old('news.published_at') ? 'checked' : '';
			?>
			<div class="field">
				<label for="published" class="checkbox">
					<input type="checkbox" name="published" id="published" value="1" <?= $checked ?> />
					Pubblicato
				</label>
			</div>
			<!-- Actions -->
			<div class="field is-grouped">
				<div class="control">
					<input type="submit" value="Salva" class="button is-primary is-large" />
				</div>
				<div class="control">
					<a href="index.php?cmd=ListNews" class="button is-secondary is-large">Annulla</a>
				</div>
			</div>
			<?= ViewHelper::csrfInput(); ?>
			<?= ViewHelper::methodInput('POST'); ?>
			<input type="hidden" name="news_id" value="<?= ViewHelper::old('news.id'); ?>" />
		</form>
	</div>
</section>
<!-- Attachments -->
<?php
if (ViewHelper::requestObject('news')->exists()) {
	ViewHelper::view('news.attachments');
}
?>