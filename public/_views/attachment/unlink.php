<?php
use \App\View\ViewHelper;
$attachment = ViewHelper::requestObject('attachment');
$news = ViewHelper::requestObject('news');
?>
<?php 
ViewHelper::view('default.unlink', [
	'title' => 'Elimina pagina',
	'message' => sprintf("Sei sicuro di voler rimuovere l'allegato '%s' dalla news '%s'?", $attachment->title, $news->subject),
	'domainObject' => $attachment,
	'relative' => $news,
	'unlinkCommand' => 'UnlinkAttachmentFromNews',
	'cancelCommand' => 'EditNews',
]); 
?>