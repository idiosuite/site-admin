<?php

namespace App\View\Attachment;

use \App\Domain\Attachment as DomainAttachment;

class FileAttachment extends AbstractAttachment
{
	public static function matchesType(DomainAttachment $attachment)
	{
		return !! ($attachment->size && $attachment->mimetype);
	}

	public function getHtml()
	{
		return sprintf($this->markup, $this->attachment->permalink, $this->text);
	}
}