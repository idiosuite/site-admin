<?php

namespace App\View\Attachment;

use \App\Domain\Attachment as DomainAttachment;

abstract class AbstractAttachment implements Attachment
{
	protected $attachment;
	protected $markup;
	protected $text;

	final public function __construct(DomainAttachment $attachment)
	{
		$this->attachment = $attachment;
	}

	public function setMarkup($markup)
	{
		$this->markup = $markup;

		return $this;
	}

	public function setText($text)
	{
		$this->text = $text;

		return $this;
	}
}