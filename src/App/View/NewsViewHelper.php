<?php

namespace App\View;

use \App\View\Attachment\AttachmentFactory;
use \App\Domain\Collection;
use \App\Domain\News;

class NewsViewHelper
{
	public static function formatDatetime(News $news)
	{
		return CalendarHelper::formatDatetime($news->display_date ?: $news->published_at);
	}

	public static function formatBody(News $news, $classes)
	{
		$body = $news->body;
		$body = explode("\n", $body);
		$body = array_map(function($b) use ($classes) {
			return '<p class="'.$classes.'">' . trim($b) . '</p>';
		}, $body);
		return implode("", $body);
	}

	public static function wrapIframe($attachments, $tag, $classes)
	{
		return str_replace(
			['<iframe', '</iframe>'], 
			[sprintf('<%s class="%s"><iframe', $tag, $classes), sprintf('</iframe></%s>', $tag)], 
			$attachments
		);
	}

	public static function blockCookies($attachments)
	{
		return CookiesHelper::iframe($attachments)->get();
	}

	public static function attachments(News $news, $text, $markup = null)
	{
		return trim(
			implode(
				' ', 
				static::attachmentsArray($news->attachments, $text, $markup)
			)
		);
	}

	public static function attachmentsArray($newsAttachments, $text, $markup = null)
	{
		$newsAttachment = static::normalizeCollection($newsAttachments);

		$attachments = [];

		foreach ($newsAttachments as $attachment) {
			$attachments[] = (new AttachmentFactory($text, $markup))->createEmbeddableAttachment($attachment)->getHtml();	
		}
		
		foreach ($newsAttachments as $attachment) {
			$attachments[] = (new AttachmentFactory($text, $markup))->createAttachment($attachment)->getHtml();	
		}

		return array_values(array_filter($attachments));
	}

	private static function normalizeCollection($collectionOrArray)
	{
		if (is_array($collectionOrArray)) {
			return new Collection($collectionOrArray);
		}

		if ( ! $collectionOrArray instanceof Collection) {
			throw new \Exception('Parameter must be either an array or a \App\Domain\Collection instance');
		}

		return $collectionOrArray;
	}

	public static function emptyNotice($collection, $text)
	{
		if ($collection->isEmpty()) {
			echo '<p>' . $text . '</p>';
		}
	}
}