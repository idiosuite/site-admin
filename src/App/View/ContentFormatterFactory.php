<?php

namespace App\View;

use \App\View\ContentFormatter;
use \App\View\Content\Title;
use \App\View\Content\Image;
use \App\View\Content\Element;
use \App\View\Content\InlineElement;
use \App\View\Content\MultilineElement;

use \App\View\Content\Tag;
use \App\View\Content\NullTag;

class ContentFormatterFactory
{
	public static function make()
	{
		return new static;
	}

	public function createStaffFormatter($content)
	{
		$formatter = new ContentFormatter;
		$formatter->setFilters([
			[Title::class, [new Tag('h4', ['class' => 'Staff__title']), '**']],
			[Title::class, [new NullTag, '*']],
			[Element::class, [new Tag('h3', ['class' => 'Staff__name Staff__name--noJobTitle']), '@']],
			[Element::class, [new Tag('p'), '-']]
		]);
		$formatter->setContent($content);
		return $formatter;
	}

	public function createServicesFormatter($content)
	{
		$formatter = new ContentFormatter;
		$formatter->setFilters([
			// Titles
			[Title::class, [new Tag('h4'), '**']],
			[Title::class, [new Tag('h3'), '*']],
			// List
			[MultilineElement::class, [new Tag('ul', ['class' => 'Services']), new Tag('li'), '-', null, $canBeNested = true]],
			// Image
			[Image::class, [new Tag('img'), '[img:', ']']],
			// Card
			[Element::class, [(new Tag('div', ['class' => 'ServicesCard__container']))->open(), '@start:cardGroup', '']],
			[Element::class, [(new Tag('div'))->close(), '@end:cardGroup', '']],
			[Element::class, [(new Tag('div', ['class' => 'ServicesCard']))->open(), '@start:card', '']],
			[Element::class, [(new Tag('div'))->close(), '@end:card', '']],
			// Paragraphs
			[Element::class, [new Tag('p'), '']],
			// Inline elements
			[InlineElement::class, [new Tag('strong'), '^']],
		]);
		$formatter->setContent($content);
		return $formatter;
	}

	public function createInfoFormatter($content)
	{
		$formatter = new ContentFormatter;
		$formatter->setFilters([
			[Title::class, [new Tag('h3'), '*']],
			[Element::class, [new Tag('p'), '-']]
		]);
		$formatter->setContent($content);
		return $formatter;
	}
}