<?php

namespace App\View\Content;

class Tag 
{
	private $tag;
	private $single = false;
	private $open = false;
	private $close = false;
	private $attributes = [];

	public function __construct($tag = '', array $attributes = [])
	{
		$this->tag = $tag;
		$this->attributes = $attributes;
	}

	public function single()
	{
		$this->single = true;
		return $this;
	}

	public function open()
	{
		$this->open = true;
		return $this;
	}

	public function close()
	{
		$this->close = true;
		return $this;
	}

	public function build($content = null)
	{
		if ($this->single) {
			return $this->getOpen();
		}
		if ($this->open) {
			return $this->getOpen();
		}
		if ($this->close) {
			return $this->getClose();
		}
		return $this->getOpen() . $content . $this->getClose();
	}

	private function getOpen()
	{
		if ( ! $this->tag) {
			return '';
		}

		return '<' . $this->tag . $this->getAttributes() . '>';
	}

	private function getAttributes()
	{
		if ( ! $this->attributes) {
			return;
		}

		$attr = [];
		
		foreach ($this->attributes as $key => $value) {
			$attr[] = $key . '="' . $value . '"';
		}

		return ' ' . implode(' ', $attr);
	}

	private function getClose()
	{
		if ( ! $this->tag) {
			return '';
		}

		return '</' . $this->tag . '>';
	}
}