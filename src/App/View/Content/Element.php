<?php

namespace App\View\Content;

class Element implements FilterInterface
{
	private $lines = [];
	private $startToken;
	private $endToken;
	private $tag;

	public function __construct(Tag $tag, $startToken, $endToken = null)
	{
		$this->tag = $tag;
		$this->startToken = $startToken;
		$this->endToken = $endToken ?: $this->startToken;
	}

	protected function getTag()
	{
		return $this->tag;
	}

	protected function getStartToken()
	{
		return $this->startToken;
	}

	protected function getEndToken()
	{
		return $this->endToken;
	}

	public function isInline()
	{
		return false;
	}

	public function isMultiline()
	{
		return false;
	}

	public function canBeNested()
	{
		return false;
	}

	public function addLine($line) 
	{
		if ($line) {
			$this->lines[] = $line;
		}

		return $this;
	}

	public function get()
	{
		$lines = $this->getLinesWithoutToken();
		
		if ($this->isMultiline()) {
			$indentHelper = new IndentHelper(
				$this->lines, 
				$this->startToken, 
				$this->canBeNested()
			);
			$indentHelper->createIndentsMap();
			return $this->convertNestedLinesToMarkup(
				$indentHelper->createNestedLinesArray($lines)
			);
		}
		
		return $this->convertLinesToMarkup($lines);
	}

	protected function getLinesWithoutToken()
	{
		return array_map([$this, 'removeLineToken'], $this->getLines());
	}

	protected function getLines()
	{
		return $this->lines;
	}

	private function removeLineToken($line)
	{
		$clean = trim($line, $this->startToken);
		
		if ($this->endToken) {
			$clean = trim($clean, $this->endToken);
		}
		
		return $clean;
	}

	private function convertNestedLinesToMarkup(array $lines)
	{
		$tmpLines = [];

		foreach ($lines as $i => $line) {
			if (is_array($line)) {
				$line = $this->convertNestedLinesToMarkup($line);
			}
			$tmpLines[$i] = $this->buildLineTag($line);
		}

		return $this->convertLinesToMarkup($tmpLines);
	}

	protected function convertLinesToMarkup(array $lines)
	{
		$lines = implode($this->getLinesSeparator(), $lines);
		return $this->tag->build($lines);
	}

	protected function getLinesSeparator()
	{
		return '';
	}

	private function buildLineTag($line)
	{
		return $this->getLineTag()->build($line);
	}

	protected function getLineTag()
	{
		throw new \Exception('Please provide a line tag');
	}

	public function matches($line)
	{
		return $this->beginsWithToken($line);
	}

	protected function beginsWithToken($line)
	{
		return substr($line, 0, strlen($this->startToken)) == $this->startToken;
	}

	protected function endsWithToken($line)
	{
		return substr($line, -strlen($this->endToken)) == $this->endToken;
	}

	public function __toString()
	{
		return $this->get();
	}
}

class IndentHelper
{
	private $lines = [];
	private $indentToken;
	private $canBeNested;

	private $linesIndent = [];
	private $currentIndent = 0;

	public function __construct(array $lines, $indentToken, $canBeNested)
	{
		$this->lines = $lines;
		$this->indentToken = $indentToken;
		$this->canBeNested = $canBeNested;
	}

	public function createIndentsMap()
	{
		$this->linesIndent = array_map([$this, 'getLineIndents'], $this->lines);
	}

	private function getLineIndents($line)
	{
		$pattern = '/^[' . $this->indentToken . ']+/';
		
		if ($this->canBeNested() && preg_match($pattern, $line, $matches)) {
			return strlen($matches[0]) - 1;
		}

		return 0;
	}

	public function createNestedLinesArray($lines)
	{
		$rootGroup = new Group;
		$currentGroup = $rootGroup;
		$previousGroups = null;

		foreach ($lines as $i => $line) {
			if ($this->currentIndent < $this->linesIndent[$i]) {
				// Store currentGroup to previousGroups
				$previousGroups[] = $currentGroup;
				// Open new group
				$currentGroup = new Group;
				$previousGroups[count($previousGroups) - 1]->add($currentGroup);
				// Append to new group
				$currentGroup->add($line);
			}
			if ($this->currentIndent == $this->linesIndent[$i]) {
				// Append to open group
				$currentGroup->add($line);
			}
			if ($this->currentIndent > $this->linesIndent[$i]) {
				$delta = $this->currentIndent - $this->linesIndent[$i] - 1;
				if ($delta) {
					$previousGroups = array_slice($previousGroups, 0, -$delta);
				}
				// Close group
				$currentGroup = $previousGroups[count($previousGroups) - 1];
				// Append to previous group
				$currentGroup->add($line);
			}
			
			$this->currentIndent = $this->linesIndent[$i];
		}
		
		return $rootGroup->toArray();
	}

	private function canBeNested()
	{
		return $this->canBeNested;
	}
}

class Group
{
	private $items = [];

	public function toArray()
	{
		$tmp = [];
		foreach ($this->items as $item) {
			if ($item instanceof Group) {
				$item = $item->toArray();
			}
			$tmp[] = $item;
		}
		return $tmp;
	}

	public function add($groupOrItem)
	{
		$this->items[] = $groupOrItem;
	}
}