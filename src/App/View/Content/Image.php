<?php

namespace App\View\Content;

class Image extends Element
{
	public function get()
	{
		$attributes = $this->getAttributes();
		$src = array_shift($attributes);
		$attributes = array_merge(['src:' . $src], $attributes);
		$attributes = $this->parseAttributes($attributes);
		return (new Tag('img', $attributes))->single()->build();
	}

	private function parseAttributes(array $attributes)
	{
		$result = [];
		foreach ($attributes as $attributeLine) {
			list ($attribute, $value) = explode(':', $attributeLine);
			$result[$attribute] = $value;
		}
		return $result;
	}

	private function getAttributes()
	{
		$lines = $this->getLinesWithoutToken();
		return explode('|', $lines[0]);
	}

	public function matches($line)
	{
		return $this->beginsWithToken($line) && $this->endsWithToken($line);
	}
}