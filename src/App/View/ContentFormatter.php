<?php

namespace App\View;

use \App\View\Content\Tag;
use \App\View\Content\Title;
use \App\View\Content\Element;
use \App\View\Content\MultilineElement;
use \App\View\Content\Paragraph;
use \App\View\Content\FilterInterface;

class ContentFormatter
{
	private $content;
	private $format;
	private $filters = [];

	public function __construct()
	{
		$this->setFilters($this->getDefaultFilters());
	}

	private function getDefaultFilters()
	{
		return [
			[Title::class, [new Tag('h2'), '**']],
			[Title::class, [new Tag('h1'), '*']],
			[Paragraph::class, [new Tag('p'), new Tag('')]]
		];
	}

	public function setFilters(array $filters = [])
	{
		$this->filters = $filters;
		return $this;
	}

	public function setContent($content)
	{
		$this->content = trim($content);
		return $this;
	}

	public function get()
	{
		if ( ! $this->content) {
			return;
		}

		$lines = $this->getLines();
		$lines = $this->parseInlineElements($lines);
		$lines = $this->parseLines($lines);
		return implode('', $lines);
	}

	private function getLines()
	{
		return array_map(
			'trim', 
			explode("\n", $this->content)
		);
	}

	private function parseInlineElements(array $lines)
	{
		foreach ($lines as $i => $line) {
			foreach ($this->getInlineFilters() as $filter) {
				$lines[$i] = $this->createFilterInstance($filter)
					->addLine($line)
					->get();
			}
		}
		
		return $lines;
	}

	private function parseLines(array $lines)
	{
		$this->outputFilters = [];
		
		foreach ($lines as $line) {
			$filterInstance = $this->getFilterForLine($line);

			if ( ! $filterInstance) {
				continue;
			}

			if ( ! $this->lineBelongsToActiveMultilineFilter($filterInstance, $line)) {
				$this->appendNewOutputFilter($filterInstance);
			}

			$this->getLastOutputFilter()->addLine($line);
		}

		return $this->outputFilters;
	}

	private function getFilterForLine($line)
	{
		foreach ($this->getLineFilters() as $filter) {
			$filterInstance = $this->createFilterInstance($filter);
			
			if ($filterInstance->matches($line)) {
				return $filterInstance;
			}
		}
	}

	private function appendNewOutputFilter(FilterInterface $filterInstance)
	{
		$this->outputFilters[] = $filterInstance;
	}

	private function lineBelongsToActiveMultilineFilter(FilterInterface $filterInstance, $line)
	{
		 return $line && 
	 		$this->hasCurrentFilter() && 
	 		$this->filterIsMultilineAndMatchesCurrentFilter($filterInstance);
	}

	private function hasCurrentFilter()
	{
		return !! $this->outputFilters;
	}

	private function filterIsMultilineAndMatchesCurrentFilter(FilterInterface $filterInstance)
	{
		if ( ! $filterInstance->isMultiline()) {
			return false;
		}

		return is_a(
			$this->getLastOutputFilter(), 
			get_class($filterInstance)
		);
	}

	private function getLastOutputFilter()
	{
		return $this->outputFilters[count($this->outputFilters) - 1];
	}

	private function getInlineFilters()
	{
		return array_filter($this->filters, function($filter) {
			return $this->createFilterInstance($filter)->isInline();
		});
	}

	private function getLineFilters()
	{
		return array_filter($this->filters, function($filter) {
			return ! $this->createFilterInstance($filter)->isInline();
		});
	}

	private function createFilterInstance(array $filter)
	{
		list ($filterClassName, $args) = $filter;
		$r = new \ReflectionClass($filterClassName);
		return $r->newInstanceArgs($args);
	}
}