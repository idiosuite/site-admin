<?php

namespace App\View\Iubenda;

abstract class Element
{
	protected $original;
	protected $suppressed;

	public function __construct($scriptMarkup)
	{
		$this->original = $scriptMarkup;
		$this->suppressed = $scriptMarkup;
		
		$this->blockCookies();
	}

	abstract protected function blockCookies();

	public function __toString()
	{
		return $this->get();
	}

	public function get()
	{
		return $this->suppressed;
	}
}