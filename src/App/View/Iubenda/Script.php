<?php

namespace App\View\Iubenda;

class Script extends Element
{
	protected function blockCookies()
	{
		$this
			->addCssClass()
			->replaceType()
			->suppressSrc();
	}

	public function fromUrls(array $urls)
	{
		$scripts = array_map(
			function($url){
				return sprintf('<script src="%s"></script>', $url);
			}, 
			$urls
		);

		$blockedScripts = array_map(
			function($scriptMarkup){
				return new static($scriptMarkup);
			}, 
			$scripts
		);

		return implode('', $blockedScripts);
	}

	private function addCssClass()
	{
		$this->suppressed = str_replace(
			'<script ', 
			'<script class="_iub_cs_activate" ', 
			$this->suppressed
		);

		return $this;
	}

	private function replaceType()
	{
		$this->suppressed = str_replace(
			'type="text/javascript"', 
			'type="text/plain"', 
			$this->suppressed
		);

		if (stripos($this->suppressed, 'type="') === false) {
			$this->suppressed = str_replace(
				'<script ', 
				'<script type="text/plain" ', 
				$this->suppressed
			);			
		}

		return $this;
	}

	private function suppressSrc()
	{
		$this->suppressed = str_replace(
			'src="', 
			'data-suppressedsrc="', 
			$this->suppressed
		);

		return $this;
	}
}