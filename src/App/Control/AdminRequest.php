<?php

namespace App\Control;

use \Illuminate\Http\Request;

use \App\Control\Request\ObjectCollector;
use \App\Control\Request\Authenticator;
use \App\Control\Request\FeedbackCollector;
use \App\Control\Request\ValidationLog;

use \App\Control\AdminAuthenticationService;

class AdminRequest
{
	use ObjectCollector;
	use Authenticator;
	use FeedbackCollector;
	use ValidationLog;

	private static $instance;

	private $request;

	public function __construct()
	{
		$this->request = Request::capture();
	}

	public function files()
	{
		return $this->request->files;
	}

	public static function instance()
	{
		if ( ! static::$instance) {
			static::$instance = new static;
		}

		return static::$instance;
	}

	public static function reset()
	{
		return static::setInstance(null);
	}

	public static function setInstance(AdminRequest $request = null)
	{
		static::$instance = $request;
	}

	public function __call($name, $args)
	{
		return call_user_func_array([$this->request, $name], $args);
	}

	public function __get($name)
	{
		return $this->request->$name;
	}

	public function __set($name, $value)
	{
		$this->request->$name = $value;
	}

	public function formWasSubmitted()
	{
		return strtolower($this->_method) == 'post';
	}

	public function method()
	{
		return $this->formWasSubmitted() ? 'POST' : $this->request->method();
	}

	public function validateCsrfToken()
	{
		return $this->token != \App\Base\AppContainer::get('session')->csrfToken;
	}

	protected function getAuthenticationService()
	{
		return new AdminAuthenticationService;
	}
}