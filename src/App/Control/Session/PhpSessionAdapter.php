<?php

namespace App\Control\Session;

class PhpSessionAdapter implements SessionAdapter
{
	public function start()
	{
		session_start();
	}

	public function regenerateId()
	{
		session_regenerate_id();
	}

	public function destroy()
	{
		$_SESSION = [];
		unset($_SESSION);
		session_destroy();
	}
	
	public function set($property, $value)
	{
		$_SESSION[$property] = $value;
	}
	
	public function get($property)
	{
		if (isset($_SESSION[$property])) {
			return $_SESSION[$property];
		}
	}
}