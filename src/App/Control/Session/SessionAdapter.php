<?php

namespace App\Control\Session;

interface SessionAdapter
{
	public function start();
	public function destroy();
	public function set($property, $value);
	public function get($property);
	public function regenerateId();
}