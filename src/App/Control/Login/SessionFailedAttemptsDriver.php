<?php

namespace App\Control\Login;

use \App\Control\Session\Session;

class SessionFailedAttemptsDriver implements FailedAttemptsDriver
{
	private $session;

	public function __construct(Session $session)
	{
		$this->session = $session;
	}

	public function reset()
	{
		$this->session->count = 0;
		$this->session->timestamp = 0;
	}

	public function increment()
	{
		++$this->session->count;
		$this->session->timestamp = time();
	}

	public function greaterThan($count, $delay)
	{
		return $this->session->count >= $count && ! $this->timeIsExpired($delay);
	}

	private function timeIsExpired($delay)
	{
		return $this->session->timestamp + $delay < time();
	}
}