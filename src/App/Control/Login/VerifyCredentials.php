<?php

namespace App\Control\Login;

class VerifyCredentials
{
	private $errorLog;
	private $username;
	private $password;

	public function __construct(ErrorLog $errorLog)
	{
		$this->errorLog = $errorLog;
	}

	public function setCredentials($username, $password)
	{
		$this->username = $username;
		$this->password = $password;
		
		return $this;
	}

	public function handle()
	{
		$result = $this->verifyCredentials();
		
		if ( ! $result) {
			$this->errorLog->addError(static::class, 'Invalid username or password');
		}
		
		return $result;
	}

	private function verifyCredentials()
	{
		return (
			password_verify($this->username, getenv('ADMIN_USERNAME')) &&
			password_verify($this->password, getenv('ADMIN_PASSWORD'))
		);
	}
}