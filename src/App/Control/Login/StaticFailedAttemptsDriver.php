<?php

namespace App\Control\Login;

class StaticFailedAttemptsDriver implements FailedAttemptsDriver
{
	private static $count = 0;
	private static $timestamp = 0;

	public function reset()
	{
		static::$count = 0;
		static::$timestamp = 0;
	}

	public function increment()
	{
		++static::$count;
		static::$timestamp = time();
	}

	public function greaterThan($count, $delay)
	{
		return static::$count >= $count && ! $this->timeIsExpired($delay);
	}

	private function timeIsExpired($delay)
	{
		return static::$timestamp + $delay < time();
	}
}