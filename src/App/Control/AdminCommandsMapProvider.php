<?php

namespace App\Control;

use \MolnApps\Control\CommandsMap\CommandsMapBuilder;

class AdminCommandsMapProvider
{
	public function getCommandsMap()
	{
		$builder = new CommandsMapBuilder(new AdminCommandStatuses, [
			'CMD_AUTH_ERROR' => 'signin',
			'CMD_INVALID_REQUEST' => 'default.invalid',
			'CMD_ERROR' => 'default.error',
			'CMD_NOT_FOUND' => 'default.notfound',
			'CMD_CHOOSE_LANG' => 'default.language',
		]);

		$builder->setGroupMiddleware('guest', ['csrf']);
		$builder->setGroupMiddleware('authenticated', ['auth', 'csrf', 'language']);

		$builder->openGroup('guest');

		$builder
			->command('SignIn')
				->on('CMD_DEFAULT', 'signin')
				->on('CMD_OK', 'dashboard')
			->command('SignOut')
				->on('CMD_OK', 'signin');

		$builder
			->command('SetLanguage')
				->on('CMD_DEFAULT', 'default.language')
				->on('CMD_OK', '@DefaultCommand');

		$builder->openGroup('authenticated');

		$builder
			->command('DefaultCommand')
				->on('CMD_DEFAULT', 'dashboard');

		$builder
			->command('ListNews')
				->on('CMD_OK', 'news.list')
			->command('EditNews')
				->on('CMD_OK', 'news.form')
			->command('NewNews')
				->alias('EditNews')
				->on('CMD_OK', 'news.form')
			->command('SaveNews')
				->middleware('post')
				->on('CMD_VALIDATION_ERROR', '@EditNews')
				->on('CMD_OK', '@ListNews')
			->command('ConfirmTrashNews')
				->alias('EditNews')
				->on('CMD_OK', 'news.trash')
			->command('TrashNews')
				->middleware('post')
				->on('CMD_OK', '@ListNews');

		$builder
			->command('ListPage')
				->on('CMD_OK', 'page.list')
			->command('EditPage')
				->on('CMD_OK', 'page.form')
			->command('NewPage')
				->alias('EditPage')
				->on('CMD_OK', 'page.form')
			->command('SavePage')
				->middleware('post')
				->on('CMD_VALIDATION_ERROR', '@EditPage')
				->on('CMD_OK', '@ListPage')
			->command('ConfirmTrashPage')
				->alias('EditPage')
				->on('CMD_OK', 'page.trash')
			->command('TrashPage')
				->middleware('post')
				->on('CMD_OK', '@ListPage');

		$builder
			->command('EditAttachment')
			->command('NewAttachment')
				->alias('EditAttachment')
				->on('CMD_OK', 'attachment.form')
			->command('SaveAttachment')
				->middleware('post')
				->on('CMD_VALIDATION_ERROR', '@NewAttachment')
				->on('CMD_OK', '@LinkAttachmentToRelative')
			->command('LinkAttachmentToRelative')
				->middleware('post')
				->on('CMD_OK', '@EditNews')
			->command('UnlinkAttachmentFromRelative')
				->middleware('post')
			->command('ConfirmUnlinkAttachmentFromNews')
				->alias('EditAttachment')
				->on('CMD_OK', 'attachment.unlink')
			->command('UnlinkAttachmentFromNews')
				->middleware('post')
				->alias('UnlinkAttachmentFromRelative')
				->on('CMD_OK', '@EditNews');

		return $builder->getCommandsMap();
	}
}