<?php

namespace App\Control\Request;

trait FeedbackCollector
{
	private $feedback = [];

	public function addSuccess($message)
	{
		return $this->addFeedback($message, 'success');
	}

	public function addError($message)
	{
		return $this->addFeedback($message, 'error');
	}

	public function addNotice($message)
	{
		return $this->addFeedback($message, 'notice');
	}

	public function getErrors()
	{
		return $this->getFeedback('error');
	}

	public function addFeedback($message, $type)
	{
		$this->feedback[] = (object)[
			'type' => $type,
			'message' => $message
		];

		return $this;
	}

	public function getFeedback($type = null)
	{
		if ($type) {
			return $this->getFeedbackWithType($type);
		}

		return $this->feedback;
	}

	private function getFeedbackWithType($type)
	{
		$result = [];

		foreach ($this->feedback as $feedback) {
			if ($feedback->type == $type) {
				$result[] = $feedback;
			}
		}

		return $result;
	}
}