<?php

namespace App\Control\Request;

use \App\Domain\Contracts\RequestObject;

trait ObjectCollector
{
	private $objects = [];
	
	public function addObject(RequestObject $object, $key = null)
	{
		$key = ($key) ?: lcfirst($object->getObjectName());
		
		$this->objects[$key] = $object;

		return $this;
	}

	public function hasObject($key)
	{
		return isset($this->objects[$key]);
	}

	public function getObject($key)
	{
		if ($this->hasObject($key)) {
			return $this->objects[$key];
		}
	}
}