<?php

namespace App\Control\Request;

trait Authenticator
{
	public function authenticate($username, $password)
	{
		return $this->getAuthenticationService()->authenticate($username, $password);
	}

	public function signOut()
	{
		return $this->getAuthenticationService()->signOut();
	}

	public function isSignedIn()
	{
		return $this->getAuthenticationService()->isSignedIn();
	}

	abstract protected function getAuthenticationService();
}