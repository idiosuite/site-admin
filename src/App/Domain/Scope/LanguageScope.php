<?php

namespace App\Domain\Scope;

use \App\Repository\QueryBuilder;

use \App\Repository\Scope\Scope;

use \App\Base\AppContainer;

class LanguageScope implements Scope
{
	public function apply(QueryBuilder $queryBuilder)
	{
		$currentLanguage = AppContainer::get('languageProvider')->getCurrentLanguage();

		if ( ! $currentLanguage) {
			throw new \Exception('No current language is set.');
		}

		$queryBuilder->where(['language' => $currentLanguage]);
	}
}