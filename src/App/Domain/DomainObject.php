<?php

namespace App\Domain;

use \Cake\Utility\Inflector;

use \App\Repository\ActiveRecord;
use \App\Repository\Relations\Relative;

use \App\Domain\Contracts\CollectionObject;
use \App\Domain\Contracts\RequestObject;

use \App\Domain\Traits\Recordable;
use \App\Domain\Traits\Traitable;
use \App\Domain\Traits\Filterable;
use \App\Domain\Traits\Findable;
use \App\Domain\Traits\Attributable;
use \App\Domain\Traits\Relatable;

abstract class DomainObject implements CollectionObject, RequestObject, ActiveRecord, Relative
{
	use Traitable;
	use Recordable;
	use Findable;
	use Attributable;
	use Relatable;
	use Filterable;

	public function __construct(array $properties = [])
	{
		$this->bootstrapTraits();
		$this->bootstrapFilters();
		$this->initializeProperties($properties);
	}

	protected function bootstrapRepository()
	{
		$this->callTraitMethod('bootstrapRepository%sTrait');
	}

	public static function make(array $properties = [])
	{
		return new static($properties);
	}

	public static function create(array $properties = [])
	{
		$instance = static::make($properties);

		$instance->save();
		
		return $instance;
	}

	public static function query()
	{
		return (new static)->getRepository();
	}

	public function __get($name)
	{
		if ( ! $this->propertyExists($name)) {
			return $this->getRelativeAsProperty($name);
		}

		return $this->getProperty($name);
	}

	private function getRelativeAsProperty($name)
	{
		if (method_exists($this, $name)) {
			return call_user_func_array([$this, $name], [])->get();
		}
	}

	public function __set($name, $value)
	{
		$this->setProperty($name, $value);
	}

	public function createCollection()
	{
		return new Collection([], new static, new static);
	}

	public function createCollectionObject(array $raw)
	{
		return new static($raw);
	}

	public function getObjectName()
	{
		$parts = explode('\\', get_class($this));
		return end($parts);
	}

	protected function filterProperty($property, $value)
	{
		return $this->filter($property, $value);
	}

	protected function getTableName()
	{
		return strtolower(Inflector::underscore(Inflector::pluralize($this->getObjectName())));
	}
}