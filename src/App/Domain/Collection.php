<?php

namespace App\Domain;

use \App\Repository\Collection as CollectionFactory;

use \App\Domain\Contracts\CollectionObject;
use \App\Domain\Contracts\RequestObject;

class Collection implements \Countable, \Iterator, RequestObject, CollectionFactory
{
	private $position = 0;

	private $raw = [];
	private $objects = [];
	
    private $collectionObject;
    private $requestObject;

	public function __construct(
        array $rows = [], 
        CollectionObject $collectionObject = null, 
        RequestObject $requestObject = null
    ) {
		$this->position = 0;
		$this->raw = $rows;
		$this->collectionObject = $collectionObject;
        $this->requestObject = $requestObject;
	}

    public function createNewCollection(array $rows = [])
    {
        return new static($rows, $this->collectionObject, $this->requestObject);
    }

    public function filterCollection($key, array $values)
    {
        $rows = [];
        
        foreach ($values as $value) {
            $match = $this->getItem($key, $value);
            if ($match) {
                $rows[] = $match;
            }
        }

        return $this->createNewCollection($rows);
    }

    private function getItem($key, $value)
    {
        foreach ($this->raw as $properties) {
            if ($properties[$key] == $value) {
                return $properties;
            }
        }
    }

    public function getObjectName()
    {
        return $this->requestObject->getObjectName() . 'Collection';
    }

	// ! Countable

	public function count()
	{
		return count($this->raw);
	}

    public function isEmpty()
    {
        return $this->count() == 0;
    }

	// ! Iterator

	public function rewind() {
        $this->position = 0;
    }

    public function current() {
        return $this->getObject($this->position);
    }

    public function key() {
        return $this->position;
    }

    public function next() {
        ++$this->position;
    }

    public function valid() {
        return isset($this->raw[$this->position]);
    }

    // ! Methods

    public function getFirst()
    {
        return (count($this) > 0) ? $this->getObject(0) : null;
    }

    public function getAt($i)
    {
        return $this->raw[$i];
    }

    public function toArray()
    {
        return $this->raw;
    }

    public function getObject($i)
    {
    	if ( ! isset($this->objects[$i])) {
    		$this->objects[$i] = $this->createObject($i);
    	}
    	
    	return $this->objects[$i];
    }

    private function createObject($i)
    {
    	return $this->collectionObject->createCollectionObject($this->raw[$i]);
    }

    public function merge(Collection $collection) : Collection
    {
        return $this->createNewCollection(
            array_merge($this->toArray(), $collection->toArray())
        );
    }
}