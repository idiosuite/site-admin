<?php

namespace App\Domain;

use \App\Domain\Traits\Publishable;
use \App\Domain\Traits\Languageable;
use \App\Base\AppContainer;

class Page extends DomainObject
{
	use Publishable;
	use Languageable;

	protected $traits = [Publishable::class, Languageable::class];

	protected $fillables = ['slug', 'title', 'heroImage', 'body'];

	protected function bootstrapFilters()
	{
		$this->setFilter('slug', \App\Validation\PlainTextFilter::class);
		$this->setFilter('title', \App\Validation\PlainTextFilter::class);
		$this->setFilter('body', \App\Validation\PlainTextFilter::class);
		$this->setFilter('heroImage', \App\Validation\PlainTextFilter::class);
	}

	public static function bySlug($slug)
	{
		$collection = Page::available()
			->where([['slug', 'in', (array)$slug]])
			->find();

		return is_array($slug) ? $collection : $collection->getFirst();
	}

	public function getFormattedBody($type = null)
	{
		$type = $type ?: $this->slug;
		$methodName = sprintf('create%sFormatter', ucfirst($type));
		$formatter = AppContainer::get('contentFormatterFactory')->$methodName($this->body);
		return $formatter->get();
	}
}