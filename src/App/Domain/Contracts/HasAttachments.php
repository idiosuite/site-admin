<?php

namespace App\Domain\Contracts;

interface HasAttachments
{
	public function attachments();
}