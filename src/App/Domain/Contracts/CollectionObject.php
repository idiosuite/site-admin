<?php

namespace App\Domain\Contracts;

interface CollectionObject
{
	public function createCollectionObject(array $raw);
}