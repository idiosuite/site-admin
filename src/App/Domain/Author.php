<?php

namespace App\Domain;

use \App\Domain\Contracts\HasAttachments;

class Author extends DomainObject
{
	protected $fillables = ['slug', 'title', 'first_name', 'last_name', 'company'];

    public function getFullName() {
        return trim($this->title . ' ' . $this->first_name . ' ' . $this->last_name);
    }

    public static function findBySlugOrFail($slug)
    {
        $authors = (new static)
            ->query()
            ->where(['slug' => $slug])
            ->find();

        if ($authors->isEmpty()) {
            throw new \Exception('Could not find author.');
        }        

        return $authors->getFirst();
    }
}