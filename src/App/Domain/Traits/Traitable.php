<?php

namespace App\Domain\Traits;

trait Traitable
{
	protected $traits = [];

	protected function bootstrapTraits()
	{
		$this->callTraitMethod('bootstrap%sTrait');
	}

	protected function callTraitMethod($template)
	{
		foreach ($this->traits as $trait) {
			$methodName = $this->getTraitMethod($trait, $template);
			if (method_exists($this, $methodName)) {
				$this->$methodName();
			}
		}
	}

	private function getTraitMethod($trait, $template)
	{
		$traitName = $this->getTraitNiceName($trait);
		return sprintf($template, ucfirst($traitName));
	}

	private function getTraitNiceName($trait)
	{
		$parts = explode('\\', $trait);
		return end($parts);
	}
}