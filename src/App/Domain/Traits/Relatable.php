<?php

namespace App\Domain\Traits;

use \Cake\Utility\Inflector;

use \App\Repository\Relations\ManyToMany;
use \App\Repository\Relations\Relative;
use \App\Repository\Map;

use \App\Database\TableGatewayFactory;

trait Relatable
{
	private $parent;
	private $relative;

	protected function belongsToMany(Relative $relative)
	{
		$this->relative = $relative;
		
		$tableGateway = TableGatewayFactory::get($this->getPivotTableName());

		return new ManyToMany($tableGateway, $this->getParent(), $this->getRelative(), $this->getRelative());
	}

	private function getPivotTableName()
	{
		$arr = [
			Inflector::pluralize($this->getParent()->getRelativeName()), 
			Inflector::pluralize($this->getRelative()->getRelativeName())
		];
		
		sort($arr);
		
		return strtolower(Inflector::underscore(implode('', $arr)));
	}

	private function getParent()
	{
		return $this;
	}

	private function getRelative()
	{
		return $this->relative;
	}

	public function getPrimaryKey()
	{
		return (new Map)->getPrimaryKey();
	}

	public function getForeignKey()
	{
		return lcfirst($this->getRelativeName()).'_id';
	}

	public function getId()
	{
		return $this->getProperty($this->getPrimaryKey());
	}

	public function getRelativeName()
	{
		return $this->getObjectName();
	}
}