<?php

namespace App\Testing;

use \org\bovigo\vfs\vfsStream;
use \org\bovigo\vfs\visitor\vfsStreamStructureVisitor;
use \org\bovigo\vfs\content\LargeFileContent;

trait InteractsWithFileSystem
{
	private $root;

	protected function bootstrapVfs()
	{
		$this->root = vfsStream::setup('root', null, [
            'uploads' => [],
            'tmp' => [],
        ]);
	}

	protected function createLargeFile()
	{
		return vfsStream::newFile('large.txt')
			->withContent(LargeFileContent::withMegabytes(10))
			->at($this->root);
	}

	protected function assertUploadsCount($count)
	{
		$structure = $this->getStructure();
        $this->assertCount($count, $structure['root']['uploads']);
	}

	private function getStructure()
	{
		return vfsStream::inspect(new vfsStreamStructureVisitor())->getStructure();
	}
}