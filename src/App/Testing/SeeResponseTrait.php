<?php

namespace App\Testing;

trait SeeResponseTrait
{
	protected function seeSignInPage()
	{
		return $this->seeTitle('Sign in');
	}

	protected function seeErrorPage()
	{
		return $this->seeTitle('Error');
	}

	protected function seeInvalidRequestPage()
	{
		return $this->seeTitle('Invalid request');
	}

	protected function seeDashboard()
	{
		return $this->seeTitle('Dashboard');
	}

	protected function seeTitle($title)
	{
		return $this->seeElement('h2')->withText($title);
	}

	protected function seeSuccessFeedback($message)
	{
		return $this->seeFeedback('success', $message);
	}

	protected function seeErrorFeedback($message)
	{
		return $this->seeFeedback('error', $message);
	}

	private function seeFeedback($type, $message)
	{
		return $this->seeElement('p.notification.is-' . $type)->withText($message);
	}

	protected function seeValidationError($identifier, $message = '')
	{
		$handle = $this
			->seeElement('p.Validation.Validation--error')
			->withAttribute('data-validation-identifier', $identifier);

		if ($message) {
			$handle->withText($message);
		}

		return $handle;
	}
}