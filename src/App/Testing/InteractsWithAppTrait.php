<?php

namespace App\Testing;

use \App\Base\AppContainer;
use \App\Control\AdminTestController;

trait InteractsWithAppTrait
{
	protected function createController()
    {
        return new AdminTestController;
    }

    /** @before */
    protected function setUpApp()
    {
        if ($this->shouldBootstrapLanguageProvider()) {
            $this->bootstrapLanguageProvider();
        }

        $this->bootApplication();
    }

    protected function shouldBootstrapLanguageProvider()
    {
        return true;
    }

    private function bootstrapLanguageProvider()
    {
        $languageProvider = AppContainer::get('languageProvider');
        if ( ! $languageProvider->getAvailableLanguages()) {
            $languageProvider->setAvailableLanguages(['it_IT' => 'Italiano']);
            $languageProvider->setCurrentLanguage('it_IT');
        }
    }

    /** @after */
    protected function tearDownApp()
    {
    	$this->shutdownApplication();
    }
}