<?php

namespace App\Testing;

trait FactoryTrait
{
	public function persist($targetClass, array $override = [])
	{
		$domainObject = $this->create($targetClass, $override);
		$domainObject->save();
		return $domainObject;
	}
	
	public function create($targetClass, array $override = [])
	{
		$default = [
			'App\Domain\News' => [
				'language' => 'it_IT',
				'type' => 'news',
				'subject' => 'Foobar',
				'body' => 'Lorem ipsum dolor sit amet',
				'display_date' => gmdate('Y-m-d H:i:s'),
			],
			'App\Domain\Page' => [
				'language' => 'it_IT',
				'slug' => 'foobar',
				'title' => 'Foobar',
				'body' => 'Lorem ipsum dolor sit amet',
				'display_date' => gmdate('Y-m-d H:i:s'),
			],
			'App\Domain\Attachment' => [
				'title' => 'Foobar',
				'filename' => 'abc123',
				'size' => '1024',
				'mimetype' => 'image/png',
				'permalink' => 'http://www.example.com/uploads/abc123.png',
			],
			'App\Domain\Author' => [
				'slug' => 'john-doe',
				'title' => 'Dr.',
				'first_name' => 'John',
				'last_name' => 'Doe',
				'company' => 'Studio Doe',
			],
		];
		$properties = array_merge($default[$targetClass], $override);
		$domainObject = new $targetClass;
		foreach ($properties as $key => $value) {
			$domainObject->$key = $value;
		}
		return $domainObject;
	}
}