<?php

namespace App\Testing;

use \App\Database\PdoProvider;

trait InteractsWithDatabaseTrait
{
    /**
    * @before
    */
    public function setupDatabase()
    {
        $tables = [
            'news', 
            'attachments', 
            'attachments_news', 
            'pages', 
            'authors',
            'authors_news'
        ];
        
        foreach ($tables as $table) {
            PdoProvider::pdo()->query(sprintf('TRUNCATE %s', $table));            
        }
    }
}