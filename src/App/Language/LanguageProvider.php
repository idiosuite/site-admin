<?php

namespace App\Language;

use \App\Base\AppContainer;

class LanguageProvider
{
	private static $availableLanguages = [];

	private $session;

	public function __construct(\App\Control\Session\Session $session)
	{
		$this->session = $session;
	}

	public function reset()
	{
		static::$availableLanguages = [];
	}

	public function getCurrentLanguage()
	{
		return $this->session->language;
	}

	public function getCurrentLanguageName()
	{
		return $this->getLanguageName($this->getCurrentLanguage());
	}

	public function getLanguageName($language)
	{
		return isset($this->getAvailableLanguages()[$language]) 
			? $this->getAvailableLanguages()[$language]
			: $language;
	}

	public function setCurrentLanguage($language)
	{
		if ($this->languageIsSupported($language)) {
			$this->session->language = $language;
		}
	}

	private function languageIsSupported($language)
	{
		return in_array($language, array_keys($this->getAvailableLanguages()));
	}

	public function getFirstAvailableLanguage()
	{
		return array_keys($this->getAvailableLanguages())[0];
	}

	public function supportsOneLanguage()
	{
		return count($this->getAvailableLanguages()) == 1;
	}

	public function supportsMultipleLanguages()
	{
		return count($this->getAvailableLanguages()) > 1;
	}

	public function setAvailableLanguages(array $availableLanguages)
	{
		static::$availableLanguages = $availableLanguages;
	}

	public function getAvailableLanguages()
	{
		return static::$availableLanguages;
	}
}