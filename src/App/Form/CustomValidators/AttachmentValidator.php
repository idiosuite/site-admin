<?php

namespace App\Form\CustomValidators;

use App\Control\AdminRequest;
use App\Domain\Attachment;
use App\Form\ValidationRule;
use App\Form\Validator;
use App\Form\Validators\RequiredUploadedFileValidator;
use App\Form\Validators\RequiredValidator;

class AttachmentValidator implements Validator
{
	public function validate(AdminRequest $request)
	{
		return 
			(new RequiredUploadedFileValidator(new Attachment, 'attachment', 'required:uploadedFile'))->validate($request) ||
			(new RequiredValidator(new Attachment, 'url', 'required'))->validate($request);
	}

	public function addValidationError(AdminRequest $request)
	{
		$request->addValidationError([
			'messageIdentifier' => 'attachment',
			'message' => 'Please provide a file or a link to be attached.',
			'identifier' => 'attachment.attachment',
		]);
	}
}