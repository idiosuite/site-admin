<?php

namespace App\Form;

use App\Control\AdminRequest;

interface Validator
{
	public function validate(AdminRequest $request);
	public function addValidationError(AdminRequest $request);
}