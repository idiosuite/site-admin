<?php

namespace App\Form;

class ValidationRule
{
	private $required;
	private $requiredUploadedFile;

	public function __construct($ruleString)
	{
		if ($ruleString == 'required') {
			$this->required = true;
		}
		if ($ruleString == 'required:uploadedFile') {
			$this->requiredUploadedFile = true;
		}
	}

	public function isRequired()
	{
		return $this->required;
	}

	public function isRequiredUploadedFile()
	{
		return $this->requiredUploadedFile;
	}
}