<?php

namespace App\Form\Validators;

use App\Control\AdminRequest;
use App\Form\ValidationRule;
use App\Form\Validator;

abstract class AbstractValidator implements Validator
{
	protected $domainObject;
	protected $field;
	protected $rule;

	public function __construct($domainObject, $field, $ruleString)
	{
		$this->domainObject = $domainObject;
		$this->field = $field;
		$this->rule = new ValidationRule($ruleString);
	}

	public function addValidationError(AdminRequest $request)
	{
		$request->addValidationError([
			'messageIdentifier' => $this->getMessageIdentifier(),
			'message' => sprintf($this->getErrorMessage($this->getMessageIdentifier()), $this->field),
			'identifier' => $this->getIdentifier($this->field),
		]);
	}

	abstract protected function getMessageIdentifier();

	private function getErrorMessage($key)
	{
		$map = [
			'required' => 'Field %s is required.',
		];

		return isset($map[$key]) ? $map[$key] : null;
	}

	private function getIdentifier($field)
	{
		return lcfirst($this->domainObject->getObjectName()) . '.' . $field;
	}
}