<?php

namespace App\Form;

use App\Form\Validators\RequiredUploadedFileValidator;
use App\Form\Validators\RequiredValidator;

class FormValidator
{
	private $request;
	private $domainObject;

	public function __construct($request, $domainObject)
	{
		$this->request = $request;
		$this->domainObject = $domainObject;
	}

	public function validate(array $rules)
	{
		foreach ($rules as $field => $rule) {
			if ($rule instanceof Validator) {
				if ( ! $result = $rule->validate($this->request)) {
					$rule->addValidationError($this->request);
				}
				return $result;
			}

			$validators = [
				RequiredValidator::class,
				RequiredUploadedFileValidator::class,
			];

			foreach ($validators as $validatorClass) {
				$validator = new $validatorClass($this->domainObject, $field, $rule);
				if ( ! $validator->validate($this->request)) {
					$validator->addValidationError($this->request);
				}
			}
		}
		
		return ! $this->request->hasValidationErrors();
	}
}