<?php

namespace App\FrontEnd\ContentProvider;

use \App\Domain\Page;

class DbContentProvider
{
	public function getPageBySlug($slugs)
	{
		return Page::bySlug($slugs);
	}
}