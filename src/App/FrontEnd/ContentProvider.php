<?php

namespace App\FrontEnd;

use \App\FrontEnd\ContentProvider\DbContentProvider;
use \App\FrontEnd\ContentProvider\StaticContentProvider;

use \App\FrontEnd\ContentProvider\StaffContentProviderHelper;

class ContentProvider
{
	private static $basePath;
	private static $driver;

	public static function setBasePath($basePath)
	{
		static::$basePath = $basePath;
	}

	public static function reset()
	{
		static::$basePath = null;
		static::$driver = null;
	}

	public static function setDriver($driver)
	{
		static::$driver = $driver;
	}

	public static function get($key)
	{
		return static::$key();
	}
	
	public static function __callStatic($name, $args)
	{
		$methodName = 'get' . ucfirst($name);
		$instance = new static;
		if (method_exists($instance, $methodName)) {
			return call_user_func_array([$instance, $methodName], $args);
		} else {
			return call_user_func_array([$instance, 'getPageBySlug'], [$name]);
		}
	}

	public function getPreferences($key = null)
	{
		$array = $this->getContentProvider('static')->getRawContentForSlug('preferences');

		if ($key && isset($array[$key])) {
			return $array[$key];
		}

		return $array;
	}

	public function getStaffList()
	{
		return (new StaffContentProviderHelper($this))->getSortedStaffArray();
	}

	public function getStaffProfile()
	{
		$slug = isset($_GET['slug']) ? $_GET['slug'] : null;
		
		return $this->getPageBySlug($slug);
	}

	public function getContacts()
	{
		return $this->getContentProvider('static')->getRawContentForSlug('contacts');
	}

	// ! Utility methods

	public function getPageBySlug($slugs)
	{
		return $this->getContentProvider()->getPageBySlug($slugs);
	}

	private function getContentProvider($driver = null)
	{
		$driver = $driver ?: static::$driver;

		return $driver == 'db'
			? (new DbContentProvider)
			: (new StaticContentProvider($this->getBasePath()));
	}

	private function getBasePath()
	{
		return static::$basePath;
	}
}