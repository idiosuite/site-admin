<?php

namespace App\Validation;

class PlainTextFilter implements Filter
{
	public function filter($value)
	{
		return filter_var($value, FILTER_SANITIZE_STRING, [
			'flags' => FILTER_FLAG_NO_ENCODE_QUOTES
		]);
	}
}