<?php

namespace App\Validation;

class LanguageFilter implements Filter
{
	public function filter($value)
	{
		return $value == 'it_IT' || $value == 'en_US' ? $value : null;
	}
}