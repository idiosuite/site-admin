<?php

namespace App\Validation;

class TimestampFilter implements Filter
{
	public function filter($value)
	{
		$pattern = '/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}/';
		return preg_match($pattern, $value) ? $value : null;
	}
}