<?php

namespace App\Validation;

interface Filter
{
	public function filter($value);
}