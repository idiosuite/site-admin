<?php

namespace App\Api;

class ApiRouter
{
    private $routes = [];

    public function register($key, $callback)
    {
        $this->routes[$key] = $callback;
    }

    public function has($key)
    {
        return array_key_exists($key, $this->routes);
    }

    public function getResponse($key = null)
    {
        $key = $this->normalizeKey($key);

        if ( ! $this->has($key)) {
            return $this->getInvalidResponse();
        }

        return $this->getCallbackResponse($key);
    }

    private function normalizeKey($key)
    {
        return $key ?: $_SERVER['REQUEST_URI'];
    }

    private function getCallbackResponse($key)
    {
        $key = $this->normalizeKey($key);

        $responseData = $this->routes[$key];

        if (is_string($responseData)) {
            return $responseData;
        }

        if (is_array($responseData)) {
            if (is_string($responseData[0])) {
                $responseData[0] = ResponseFactory::make($responseData[0]);
            }
        }

        if (is_callable($responseData)) {
            $responseData = $responseData();
        }

        if ($responseData instanceof \App\Api\ResponseData) {
            return $responseData
                ->toApiResponse()
                ->toJson();
        }
        
        return $responseData;
    }

    private function getInvalidResponse()
    {
        return (new ApiResponse)
            ->invalidate('Unknown api endpoint')
            ->toJson();
    }
}