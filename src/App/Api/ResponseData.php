<?php

namespace App\Api;

use App\Api\Contracts\Arrayable;
use App\Api\Contracts\Jsonable;

class ResponseData implements Arrayable
{
    private $data = [];

    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function toApiResponse()
    {
        return new ApiResponse($this);
    }

    public function toArray() : array {
        return array_map(function ($resource) {
            if ($resource instanceof Arrayable) {
                return $resource->toArray();
            }

            if ( ! is_array($resource)) {
                throw new \Exception('$resource should either be a Resource instance or an array.');
            }

            return $resource;
        }, $this->data);
    }
}