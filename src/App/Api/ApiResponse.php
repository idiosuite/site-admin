<?php

namespace App\Api;

use App\Api\Contracts\Arrayable;
use App\Api\Contracts\Jsonable;

class ApiResponse implements Arrayable, Jsonable
{
    private $ok = true;
    private $message = '';
    private $responseData = [];

    public function __construct(ResponseData $responseData = null)
    {
        $this->responseData = $responseData ?: new ResponseData;
    }

    public function toJson() : string {
        return json_encode(
            $this->toArray()
        );
    }

    public function invalidate($message = '')
    {
        $this->ok = false;
        $this->message = $message;

        return $this;
    }
    
    public function toArray() : array {
        $data = [];

        try {
            $data = $this->responseData->toArray();
        } catch (\Exception $e) {
            $this->invalidate();
        }
        
        return [
            'ok' => $this->ok,
            'message' => $this->message,
            'data' => $data,
        ];
    }
}