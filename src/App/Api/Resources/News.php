<?php

namespace App\Api\Resources;

use App\Domain\News as DomainNews;
use App\View\NewsViewHelper;

class News extends AbstractResource 
{
    private $news;

    public function __construct(DomainNews $news = null) {
        $this->news = $news;
    }

    public function toArray() : array {
        return [
            'id' => $this->news->getProperty('id'),
            'type' => $this->news->getProperty('type'),
            'date' => $this->news->display_date ?: $this->news->published_at,
            'date_formatted' => $this->getFormattedDate(),
            'title' => $this->news->getProperty('subject'),
            'excerpt' => $this->news->getExcerpt(),
            'body' => $this->news->getProperty('body'),
            'sources' => $this->getSources(),
            'attachments' => $this->getAttachments(),
            'path' => sprintf('/news/%d', $this->news->getId()),
        ];
    }

    private function getFormattedDate() {
        return NewsViewHelper::formatDateTime($this->news);
    }

    private function getSources() {
        return (new NewsSource($this->news))->toArray();
    }

    private function getAttachments() {
        return (new NewsAttachmentCollection($this->news))->toArray();
    }
}