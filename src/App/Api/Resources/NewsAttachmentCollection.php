<?php

namespace App\Api\Resources;

use App\Domain\News as DomainNews;
use App\View\NewsViewHelper;

class NewsAttachmentCollection extends AbstractResource
{
    private $news;

    public function __construct(DomainNews $news)
    {
        $this->news = $news;
    }

    public function toArray() : array
    {
        $attachments = [];

        foreach ($this->news->attachments as $attachment) {
            $attachments[] = (new Attachment($attachment))->toArray();
        }

        $attachments = $this->moveYoutubeAttachmentsAtTheTop($attachments);

        return $attachments;
    }

    private function moveYoutubeAttachmentsAtTheTop($attachments)
    {
        usort($attachments, function ($a, $b) {
            if ( ! $this->isYoutube($a) && $this->isYoutube($b)) {
                return 1;
            } 
            if ($this->isYoutube($a) && ! $this->isYoutube($b)) {
                return -1;
            }
            return 0;
        });

        return $attachments;
    }

    private function isYoutube($attachment)
    {
        return $attachment['type'] == 'youtube';
    }
}