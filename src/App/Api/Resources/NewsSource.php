<?php

namespace App\Api\Resources;

use \App\Domain\News as DomainNews;

class NewsSource extends AbstractResource 
{
    private $news;

    public function __construct(DomainNews $news)
    {
        $this->news = $news;
    }

    public function toArray() : array {
        $result = [];

        foreach ($this->news->authors as $author) {
            $result[] = [
                'slug' => $author->slug,
                'author' => $author->getFullName(),
                'company' => $author->company
            ];
        }
        
        return $result;
    }
}