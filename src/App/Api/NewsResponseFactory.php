<?php

namespace App\Api;

use App\Domain\News;
use App\Api\Resources\News as NewsResource;

class NewsResponseFactory
{
    const TYPES_ALL = null;
    const TYPES_NEWS = 'news';
    const TYPES_EVENTS = 'event';

    public function getAllPublished($limit = 50, $type = null) : ResponseData {
        $types = $type 
            ? (array)$type
            : $this->getAvailableTypes();

        $newsCollection = [];
        foreach ($types as $type) {
            $collection = News::publishedWithType($type, $limit);
            
            $newsCollection = $newsCollection
                ? $newsCollection->merge($collection)
                : $collection;
        }

        $response = [];

        foreach ($newsCollection as $news) {
            $response[] = new NewsResource($news);
        }


        return new ResponseData($response);
    }

    private function getAvailableTypes()
    {
        return [static::TYPES_NEWS, static::TYPES_EVENTS];
    }
}