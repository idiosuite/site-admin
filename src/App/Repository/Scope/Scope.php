<?php

namespace App\Repository\Scope;

use \App\Repository\QueryBuilder;

interface Scope
{
	public function apply(QueryBuilder $queryBuilder);
}