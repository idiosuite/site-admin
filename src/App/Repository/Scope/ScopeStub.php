<?php

namespace App\Repository\Scope;

use \App\Repository\QueryBuilder;

class ScopeStub implements Scope
{
	private $where;

	public function __construct(array $where)
	{
		$this->where = $where;
	}

	public function apply(QueryBuilder $queryBuilder)
	{
		$queryBuilder->where($this->where);
	}
}