<?php

namespace App\Repository\Relations;

interface Relation
{
	public function attach(Relative $relative = null);
	public function detach(Relative $relative = null);
	public function reset();
	public function get();
}