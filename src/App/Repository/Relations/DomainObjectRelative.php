<?php

namespace App\Repository\Relations;

use \App\Domain\DomainObject;

class DomainObjectRelative implements Relative
{
	private $domainObject;

	public function __construct(DomainObject $domainObject)
	{
		$this->domainObject = $domainObject;
	}

	public function getId()
	{
		return $this->domainObject->id;
	}

	public function getForeignKey()
	{
		return lcfirst($this->getObjectName()).'_id';
	}
}