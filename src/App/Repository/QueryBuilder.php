<?php

namespace App\Repository;

use \App\Repository\Scope\Scope;

class QueryBuilder
{
	private $columns = [];
	private $where = [];
	private $order = [];
	private $limit;
	private $offset;
	private $page;
	
	public static function create()
	{
		return new static;
	}

	public function columns(array $columns)
	{
		$this->columns = $columns;

		return $this;
	}

	public function where(array $where)
	{
		$this->where = array_merge($this->where, $where);

		return $this;
	}

	public function orderBy(array $order)
	{
		$this->order = array_merge($this->order, $order);

		return $this;
	}

	public function limit($limit)
	{
		$this->limit = $limit;

		return $this;
	}

	public function offset($offset)
	{
		$this->offset = $offset;

		return $this;
	}

	public function page($page)
	{
		if ( ! $this->limit) {
			throw new \Exception('Please provide a limit first.');
		}

		$offset = ($page - 1) * $this->limit;

		return $this->offset($offset);
	}

	public function getQuery()
	{
		return new Query($this->columns, $this->where, $this->order, $this->limit, $this->offset);
	}
}