<?php

namespace App\Repository;

interface Assignments 
{
	public function getAssignments();
	public function getAssignment($property);
	public function setAssignment($property, $value);
	public function isDirty($property);
}