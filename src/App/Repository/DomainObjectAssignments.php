<?php

namespace App\Repository;

use \App\Domain\DomainObject;

class DomainObjectAssignments implements Assignments
{
	private $domainObject;
	
	public function __construct(DomainObject $domainObject)
	{
		$this->domainObject = $domainObject;
	}

	public function getAssignments()
	{
		return $this->domainObject->getFillableProperties();
	}

	public function getAssignment($property)
	{
		return $this->domainObject->getProperty($property);
	}

	public function setAssignment($property, $value)
	{
		return $this->domainObject->setProperty($property, $value);
	}

	public function isDirty($property)
	{
		return $this->domainObject->isDirty($property);
	}
}