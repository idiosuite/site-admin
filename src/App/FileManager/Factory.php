<?php

namespace App\FileManager;

use \App\Base\Environment;

use \MolnApps\FileManager\FileManager;
use \MolnApps\FileManager\Storage\Drive\DriveStorage;
use \MolnApps\FileManager\Storage\Drive\TestDriveStorage;

class Factory
{
	public static function createFileManager()
	{
		$storage = Environment::instance()->env('test')
			? new TestDriveStorage(config()->uploads->basePath, config()->uploads->baseUrl)
			: new DriveStorage(config()->uploads->basePath, config()->uploads->baseUrl);

		$validator = new Validator;
		
		return new FileManager($storage, $validator);
	}

	public static function createUploadedFile($uploadedFile)
	{
		return new BaseUploadedFile($uploadedFile);
	}
}