<?php

namespace App\Base;

class Environment
{
	private static $instance;

	private $environment = [];
	private $value;

	public static function instance()
	{
		if ( ! static::$instance) {
			static::$instance = new static;
		}

		return static::$instance;
	}

	public static function setInstance(Environment $environment = null)
	{
		static::$instance = $environment;
	}

	public static function reset()
	{
		static::setInstance(null);
	}

	public function registerEnvironment($environment, array $values) 
	{
		$this->environment[$environment] = $values;
	}

	public function registerValue($value)
	{
		$this->value = $value;
	}

	public function env($string = null) {
		if ($string) {
			return $this->env() == $string;
		}

		foreach ($this->environment as $environment => $testValues) {
			if (in_array($this->value, $testValues)) {
				return $environment;
			}
		}
		
		return 'production';
	}
}