<?php

namespace App\Database;

use \MolnApps\Database\TableGateway as BaseTableGateway;
use \MolnApps\Database\TableGatewayFactory as BaseTableGatewayFactory;
use \MolnApps\Database\Dsn;

class TableGatewayFactory
{
	private static $driver;
	private static $registered = [];

	public static function register($table, TableGateway $tableGateway = null)
	{
		$tableGateway = ($tableGateway) ?: static::create($table);
		
		static::$registered[$table] = $tableGateway;

		return $tableGateway;
	}

	public static function reset()
	{
		static::$registered = [];
	}

	public static function get($table, Dsn $dsn = null)
	{
		if (isset(static::$registered[$table])) {
			return static::$registered[$table];
		}

		return static::create($table, $dsn);
	}

	public static function create($table, Dsn $dsn = null)
	{
		if ( ! $dsn) {
			$dsn = new Dsn(Config::driver(), PdoProvider::pdo());
		}

		return new TableGateway((new BaseTableGatewayFactory)->createTable($table, $dsn));
	}
}