<?php

namespace App\Database;

use \MolnApps\Database\TableGateway as BaseTableGateway;
use \MolnApps\Database\Dsn;

use \App\Repository\TableGateway as TableGatewayInterface;

class TableGateway implements TableGatewayInterface
{
	private $tableGateway;

	public function __construct(BaseTableGateway $tableGateway)
	{
		$this->tableGateway = $tableGateway;
	}

	public function select(array $query = [])
	{
		return $this->tableGateway->select($query);
	}
	
	public function insert(array $assignments)
	{
		return $this->tableGateway->insert($assignments);
	}
	
	public function lastInsertId()
	{
		return $this->tableGateway->lastInsertId();
	}
	
	public function update(array $assignments, array $query)
	{
		return $this->tableGateway->update($assignments, $query);
	}
	
	public function delete(array $query)
	{
		return $this->tableGateway->delete($query);
	}
}