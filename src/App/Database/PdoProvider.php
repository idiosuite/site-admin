<?php

namespace App\Database;

use \MolnApps\Database\PdoFactory;

class PdoProvider
{
	private static $pdo;

	public static function pdo()
	{
		if ( ! static::$pdo) {
			static::$pdo = PdoFactory::createPdo(Config::driver(), Config::dsn());
		}
		return static::$pdo;
	}

	public static function reset()
	{
		static::$pdo = null;
	}
}