<?php

namespace App\Commands;

use \App\Domain\Attachment;
use \App\Domain\Contracts\HasAttachments;
use \App\Domain\News;

abstract class AbstractRelativeAttachmentCommand extends AbstractCommand
{
	protected function doExecute()
	{
		$attachment = $this->getAttachment();
		$relative = $this->getRelative();

		$this->performOperation($attachment, $relative);

		return $this->statuses('CMD_OK');
	}

	abstract protected function performOperation(Attachment $attachment, HasAttachments $relative);
	
	private function getAttachment()
	{
		if ( ! $this->request->hasObject('attachment') && $this->request->id) {
			$attachment = Attachment::findOrCreate($this->request->id);
			$this->request->addObject($attachment);
		}

		return $this->request->getObject('attachment');
	}

	private function getRelative()
	{
		if ( ! $this->request->hasObject('news') && $this->request->news_id) {
			$news = News::findOrCreate($this->request->news_id);
			$this->request->addObject($news);
		}

		return $this->request->getObject('news');
	}
}