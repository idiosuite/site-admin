<?php

namespace App\Commands;

use \App\Domain\Page;

class ListPage extends AbstractCommand
{
	protected function doExecute()
	{
		$pageCollection = Page::available()->orderBy(['title' => 'asc'])->find();

		$this->request->addObject($pageCollection);

		return $this->statuses('CMD_OK');
	}
}