<?php

namespace App\Commands;

use \MolnApps\Control\Command\CommandFactory as CommandFactoryInterface;
use \App\Control\AdminRequest;

class CommandsFactory implements CommandFactoryInterface
{
	private $request;

	public function __construct(AdminRequest $request)
	{
		$this->request = $request;
	}

	public function createCommand($commandName)
	{
		$className = '\App\Commands\\' . $commandName;
		
		if ( ! class_exists($className)) {
			throw new \Exception("Command class {$commandName} does not exist");
		}

		return new $className($this->request);
	}
}