<?php

namespace App\Commands;

use \App\Domain\Page;

class SavePage extends AbstractCommand
{
	protected function doExecute()
	{
		if ( ! $this->validatePage()) {
			$this->request->addError('Missing required fields.');
			return $this->statuses('CMD_VALIDATION_ERROR');
		}

		$page = Page::findOrCreate($this->request->page_id);

		$page->language = $this->getCurrentLanguage();
		$page->slug = $this->request->slug;
		$page->title = $this->request->title;
		$page->body = $this->request->body;
		
		$page->isPublished( !! $this->request->published);

		$this->request->addObject($page);
		
		$page->save();

		return $this->statuses('CMD_OK');
	}

	private function validatePage()
	{
		return $this->request->validate([
			'slug' => 'required',
			'title' => 'required',
			'body' => 'required',
		], new Page);
	}
}