<?php

namespace App\Commands;

use \MolnApps\Control\Command\Command as CommandInterface;

use \App\Control\AdminCommandStatuses;

use \App\Control\AdminRequest;

use \App\Base\AppContainer;

abstract class AbstractCommand implements CommandInterface
{
	protected $request;
	private $status;

	public function __construct(AdminRequest $request)
	{
		$this->request = $request;
	}

	public function execute()
	{
		return $this->doExecute();
	}

	abstract protected function doExecute();

	public function getCommandStatuses()
	{

	}

	public function getStatus()
	{
		return $this->status;
	}

	public function setStatus($commandStatus)
	{
		$this->status = $commandStatus;
	}

	public function statuses($commandStatusStr = 'CMD_DEFAULT')
	{
		return $this->setStatus((new AdminCommandStatuses)->getStatus($commandStatusStr));
	}

	protected function getCurrentLanguage()
	{
		return AppContainer::get('languageProvider')->getCurrentLanguage();
	}
}