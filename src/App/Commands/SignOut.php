<?php

namespace App\Commands;

class SignOut extends AbstractCommand
{
	protected function doExecute()
	{
		if ( ! $this->request->isSignedIn()) {
			return $this->statuses('CMD_OK');
		}

		$this->request->signOut();
		
		if ($this->request->isSignedIn()) {
			return $this->statuses('CMD_ERROR');
		}

		return $this->statuses('CMD_OK');
	}
}