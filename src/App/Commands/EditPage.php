<?php

namespace App\Commands;

use \App\Domain\Page;

class EditPage extends AbstractCommand
{
	protected function doExecute()
	{
		$page = Page::findOrCreate($this->request->page_id);

		if ($page->isNew() && $this->request->page_id) {
			return $this->statuses('CMD_NOT_FOUND');
		}
		
		$this->request->addObject($page);

		return $this->statuses('CMD_OK');
	}
}