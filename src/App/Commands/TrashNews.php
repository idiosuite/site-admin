<?php

namespace App\Commands;

use \App\Domain\News;

class TrashNews extends AbstractCommand
{
	protected function doExecute()
	{
		try {
			$news = News::findOrFail($this->request->news_id);
		} catch (\Exception $e) {
			return $this->statuses('CMD_NOT_FOUND');
		}

		$news->trash();

		$this->request->addObject($news);
		$this->request->addSuccess('La news è stata eliminata.');

		return $this->statuses('CMD_OK');
	}
}