<?php

namespace App\Commands;

class SignIn extends AbstractCommand
{
	protected function doExecute()
	{
		if ( $this->request->isSignedIn()) {
			return $this->statuses('CMD_OK');
		}

		if ( ! $this->request->formWasSubmitted()) {
			return $this->statuses('CMD_DEFAULT');
		}

		$this->request->authenticate($this->request->username, $this->request->password);

		if ( $this->request->isSignedIn()) {
			return $this->statuses('CMD_OK');
		}
	}
}