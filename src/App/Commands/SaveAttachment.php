<?php

namespace App\Commands;

use \App\Form\CustomValidators\AttachmentValidator;
use \App\Domain\Attachment;
use \App\FileManager\Factory;
use \MolnApps\FileManager\Contracts\StoredFile;

class SaveAttachment extends AbstractCommand
{
	private $fileManager;

	protected function doExecute()
	{
		if ( ! $this->validateAttachment()) {
			return $this->returnValidationError();
		}

		$destinationFile = $this->shouldUploadFile() ? $this->uploadFile() : null;
		
		if ($this->fileUploadFailed($destinationFile)) {
			$this->transferValidationErrors();
			return $this->returnValidationError();
		}

		$attachment = $this->saveAttachment($destinationFile);

		$this->request->addObject($attachment);

		return $this->statuses('CMD_OK');
	}

	private function validateAttachment()
	{
		return $this->request->validate([
			'title' => 'required',
			new AttachmentValidator,
		], new Attachment);
	}

	private function returnValidationError()
	{
		$this->request->addError('Missing required fields.');
		return $this->statuses('CMD_VALIDATION_ERROR');
	}

	private function shouldUploadFile()
	{
		return $this->request->hasFile('attachment');
	}

	private function fileUploadFailed($destinationFile)
	{
		return $this->shouldUploadFile() && ! $destinationFile;
	}

	private function uploadFile()
	{
		$uploadedFile = Factory::createUploadedFile($this->request->file('attachment'));

		$this->fileManager = Factory::createFileManager();

		return $this->fileManager->store($uploadedFile);
	}

	private function transferValidationErrors()
	{
		foreach ($this->fileManager->getValidationErrors() as $type => $message) {
			$this->request->addValidationError([
				'message' => $message,
				'identifier' => 'attachment.attachment',
				'messageIdentifier' => $type,
			]);
		}
	}

	private function saveAttachment(StoredFile $destinationFile = null)
	{
		$attachment = new Attachment;
		
		$attachment->title = $this->request->title;

		if ($destinationFile) {
			$attachment->setProperties($destinationFile->toArray());
		} else {
			$attachment->permalink = $this->request->url;
		}
		
		$attachment->save();

		return $attachment;
	}
}