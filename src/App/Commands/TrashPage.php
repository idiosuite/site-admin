<?php

namespace App\Commands;

use \App\Domain\Page;

class TrashPage extends AbstractCommand
{
	protected function doExecute()
	{
		try {
			$page = Page::findOrFail($this->request->page_id);
		} catch (\Exception $e) {
			return $this->statuses('CMD_NOT_FOUND');
		}

		$page->trash();

		$this->request->addObject($page);
		$this->request->addSuccess('La pagina è stata eliminata.');

		return $this->statuses('CMD_OK');
	}
}