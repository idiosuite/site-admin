<?php

namespace App\Commands\Middleware;

use \MolnApps\Control\Middleware\Middleware;

use \App\Control\AdminCommandStatuses;

class MethodMiddleware implements Middleware
{
	private $request;
	private $method;

	public function __construct($request, $method)
	{
		$this->request = $request;
		$this->method = $method;
	}
	
	public function authorize()
	{
		if (strtolower($this->request->method()) != strtolower($this->method)) {
			$this->request->addError('Invalid request method for this command.');
			return (new AdminCommandStatuses)->getStatus('CMD_INVALID_REQUEST');
		}
	}
}