<?php

namespace App\Commands\Middleware;

use \MolnApps\Control\Middleware\Middleware;

use \App\Control\AdminCommandStatuses;
use \App\Control\AdminAuthenticationService;

class AuthMiddleware implements Middleware
{
	public function authorize()
	{
		if ( ! (new AdminAuthenticationService)->isSignedIn()) {
			return (new AdminCommandStatuses)->getStatus('CMD_AUTH_ERROR');
		}
	}
}