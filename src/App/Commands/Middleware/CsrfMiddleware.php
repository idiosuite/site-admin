<?php

namespace App\Commands\Middleware;

use \MolnApps\Control\Middleware\Middleware;

use \App\Control\AdminCommandStatuses;

class CsrfMiddleware implements Middleware
{
	private $request;

	public function __construct($request)
	{
		$this->request = $request;
	}
	
	public function authorize()
	{
		if ($this->request->method() != 'GET' && $this->request->validateCsrfToken()) {
			$this->request->addError('Csrf token could not be verified.');
			return (new AdminCommandStatuses)->getStatus('CMD_INVALID_REQUEST');
		}
	}
}