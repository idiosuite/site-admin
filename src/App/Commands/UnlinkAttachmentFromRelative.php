<?php

namespace App\Commands;

use \App\Domain\Attachment;
use \App\Domain\Contracts\HasAttachments;

class UnlinkAttachmentFromRelative extends AbstractRelativeAttachmentCommand
{
	protected function performOperation(Attachment $attachment, HasAttachments $relative)
	{
		$relative->attachments()->detach($attachment);
		$this->request->addSuccess("L'allegato è stato rimosso.");
	}
}