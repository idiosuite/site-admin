<?php

namespace App\Commands;

use \App\Domain\Attachment;
use \App\Domain\Contracts\HasAttachments;

class LinkAttachmentToRelative extends AbstractRelativeAttachmentCommand
{
	protected function performOperation(Attachment $attachment, HasAttachments$relative)
	{
		$relative->attachments()->attach($attachment);
	}
}