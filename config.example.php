<?php

$config = [];

// admin
$config['ADMIN_USERNAME'] = '$2y$12$./XEt8zc8.52KuRkzvJfMe.Q5Pa/K/teGieseiwSoe7A1jB5Urwxu';
// secret
$config['ADMIN_PASSWORD'] = '$2y$12$uHnC1YVBvH8/mf54txLQUeZSeJyxOef7vyhyJPXTh6VhwNWyXDw2q';

$config['DB_DRIVER'] = "mysql";
$config['DB_HOST'] = "127.0.0.1";
$config['DB_NAME'] = "dbname";
$config['DB_USERNAME'] = "username";
$config['DB_PASSWORD'] = "password";

foreach ($config as $key => $value) {
	putenv($key . '=' . $value);
}